<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use Kofenium\FilterData;
use Kofenium\UploadedFile;

/**
 * Safe data retrieving from $_GET, $_POST, $_COOKIE, $_SERVER AND $_ENV arrays
 */
class Input
{
    /**
     * The $_GET array
     *
     * @var array
     */
    protected static $get = [];

    /**
     * The $_POST array
     *
     * @var array
     */
    protected static $post = [];

    /**
     * The $_COOKIE array
     *
     * @var array
     */
    protected static $cookies = [];

    /**
     * The $_SERVER array
     *
     * @var array
     */
    protected static $server = [];

    /**
     * The $_ENV array
     *
     * @var array
     */
    protected static $env = [];

    /**
     * The $_FILES array
     *
     * @var array
     */
    protected static $files = [];

    /**
     * Default ordering of the arrays, when used 'all'-methods
     *
     * @var array
     */
    protected static $arrayOrder = [
        // first, highest priority
        'get',
        'post',
        'cookies',
        'server',
        'env',
        'files',
        // last, lowest priority
    ];

    /**
     * Save array name where requested variable was found, when 'all' is used
     *
     * @var string
     */
    protected static $foundAt = '';

    /**
     * Set variables to GET array
     *
     * @param array $input The input array to be stored into $get
     */
    public static function setGet(array $input)
    {
        self::setVariable($input, 'get');
    }

    /**
     * Set variables to POST array
     *
     * @param array $input The input array to be stored into $post
     */
    public static function setPost(array $input)
    {
        self::setVariable($input, 'post');
    }

    /**
     * Set variables to COOKIE array
     *
     * @param array $input The input array to be stored into $cookie
     */
    public static function setCookie(array $input)
    {
        self::setVariable($input, 'cookies');
    }

    /**
     * Set variables to SERVER array
     *
     * @param array $input The input array to be stored into $server
     */
    public static function setServer(array $input)
    {
        self::setVariable($input, 'server');
    }

    /**
     * Set variables to ENV array
     *
     * @param array $input The input array to be stored into $env
     */
    public static function setEnv(array $input)
    {
        self::setVariable($input, 'env');
    }

    /**
     * Set variables to FILES array
     *
     * @param array $input The input array to be stored into $env
     */
    public static function setFile(array $input)
    {
        self::setVariable($input, 'files');
    }

    /**
     * Set variables at once
     *
     * @param array $input The input array as 'key' => 'value' array, where
     *     'key' is one of the 'get', 'post', 'cookies', 'server' or 'env',
     *      and the 'value' is the input array
     */
    public static function setAll(array $input)
    {
        self::setVariables($input);
    }

    /**
     * Check if index exists in GET array
     *
     * @param string $uid Unique identification (name or integer index)
     * @return bool
     */
    public static function hasGet($uid)
    {
        return self::hasVariable($uid, 'get');
    }

    /**
     * Check if index exists in POST array
     *
     * @param string $uid Unique identification (name or integer index)
     * @return bool
     */
    public static function hasPost($uid)
    {
        return self::hasVariable($uid, 'post');
    }

    /**
     * Check if index exists in COOKIE array
     *
     * @param string $uid Unique identification (name or integer index)
     * @return bool
     */
    public static function hasCookie($uid)
    {
        return self::hasVariable($uid, 'cookies');
    }

    /**
     * Check if index exists in SERVER array
     *
     * @param string $uid Unique identification (name or integer index)
     * @return bool
     */
    public static function hasServer($uid)
    {
        return self::hasVariable($uid, 'server');
    }

    /**
     * Check if index exists in ENV array
     *
     * @param string $uid Unique identification (name or integer index)
     * @return bool
     */
    public static function hasEnv($uid)
    {
        return self::hasVariable($uid, 'env');
    }

    /**
     * Check if index exists in FILES array
     *
     * @param string $uid Unique identification (name or integer index)
     * @return bool
     */
    public static function hasFile($uid)
    {
        return self::hasVariable($uid, 'files');
    }

    /**
     * Check if index exists in all arrays (GET, POST, COOKIE)
     *
     * @param string $uid Unique identification (name or integer index)
     * @return bool
     */
    public static function hasAll($uid)
    {
        return self::hasVariable($uid, 'all');
    }

    /**
     * Get value from the GET array
     *
     * @param string $uid|null Unique identification (name or integer index)
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    public static function get($uid = null, $default = null, $filter = null)
    {
        return self::getVariable($uid, 'get', $default, $filter);
    }

    /**
     * Get value from the POST array
     *
     * @param string|null $uid Unique identification (name or integer index)
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    public static function post($uid = null, $default = null, $filter = null)
    {
        return self::getVariable($uid, 'post', $default, $filter);
    }

    /**
     * Get value from the COOKIE array
     *
     * @param string|null $uid Unique identification (name or integer index)
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    public static function cookie($uid = null, $default = null, $filter = null)
    {
        return self::getVariable($uid, 'cookies', $default);
    }

    /**
     * Get value from the SERVER array
     *
     * @param string|null $uid Unique identification (name or integer index)
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    public static function server($uid = null, $default = null, $filter = null)
    {
        return self::getVariable($uid, 'server', $default);
    }

    /**
     * Get value from the ENV array
     *
     * @param string|null $uid Unique identification (name or integer index)
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    public static function env($uid = null, $default = null, $filter = null)
    {
        return self::getVariable($uid, 'env', $default);
    }

    /**
     * Get value from the FILE array
     *
     * @param string|null $uid Unique identification (name or integer index)
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    public static function file($uid = null, $default = null, $filter = null)
    {
        return self::getVariable($uid, 'files', $default);
    }

    /**
     * Get value from all arrays, i.e. GET, POST, COOKIE, SERVER, ENV
     *
     * @param string|null $uid Unique identification (name or integer index)
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    public static function all($uid = null, $default = null, $filter = null)
    {
        return self::getVariable($uid, 'all', $default);
    }

    /**
     * Get value from all arrays, i.e. GET, POST, COOKIE, SERVER, ENV in separated indexes
     *
     * @param string|null $uid Unique identification (name or integer index)
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    public static function allSeparated($uid = null, $default = null, $filter = null)
    {
        $result = [];

        foreach (self::$arrayOrder as $name) {
            $result[$name] = self::getVariable($uid, $name, $default);
        }

        return $result;
    }

    /**
     * Set data of specific array
     *
     * @param Array $input Input array
     * @param string $type The array type: one of the 'post', 'get', 'cookie', 'server', 'env' or 'files'
     */
    protected static function setVariable($input, $type)
    {
        if ($type === 'files') {
            $files = [];
            $rearrange = function(array $array) use (&$rearrange) {
                if (!is_array(reset($array))) {
                    return $array;
                }

                $result = [];
                foreach($array as $property => $values) {
                    foreach($values as $key => $value) {
                        $result[$key][$property] = $value;
                    }
                }

                foreach($result as &$value) {
                    $value = $rearrange($value);
                }

                return $result;
            };
            $makeObject = function(array $array) use (&$makeObject) {
                $result = [];

                foreach ($array as $key => $value) {
                    if (!isset($value['error']) || is_array($value['error'])) {
                        $result[$key] = $makeObject($value);
                        continue;
                    }

                    $result[$key] = new UploadedFile(
                        $value['tmp_name'],
                        $value['name'],
                        $value['size'],
                        $value['type'],
                        $value['error']
                    );
                }

                return $result;
            };

            foreach ((array) $input as $name => $data) {
                if (!is_array($data['error'])) {
                    $files[$name] = new UploadedFile(
                        $data['tmp_name'],
                        $data['name'],
                        $data['size'],
                        $data['type'],
                        $data['error']
                    );
                } else {
                    $files[$name] = $makeObject($rearrange($data));
                }
            }
            $input = $files;
        }

        self::${$type} = $input;
    }

    /**
     * Set data of specific array
     *
     * @param Array $input Input array, as 'key' => 'value', where 'key' is
     *    one of the 'post', 'get', 'cookies', 'server', 'env' or 'files',
     *    and the 'value' is the input array
     */
    protected static function setVariables($input)
    {
        foreach ($input as $type => $value) {
            if (!in_array($type, self::$arrayOrder) || !is_array($value)) {
                continue;
            }

            self::setVariable($value, $type);
        }
    }

    /**
     * Check if index exists
     *
     * @param string $uid Unique identification (name or integer index)
     * @param string $type The array type: one of the 'post', 'get', 'cookie', 'server', 'env' or 'all'
     * @return bool
     */
    protected static function hasVariable($uid, $type)
    {
        if ($type === 'all') {
            self::$foundAt = '';

            foreach (self::$arrayOrder as $type) {
                if (array_key_exists($uid, self::${$type})) {

                    // save where the variable was found
                    self::$foundAt = $type;

                    return true;
                }
            }
        }

        return array_key_exists($uid, self::${$type});
    }

    /**
     * Get variable from specific array
     *
     * @param string $uid Unique identification (name or integer index)
     * @param string $type The array type: one of the 'post', 'get', 'cookie', 'server', 'env' or 'files'
     * @param mixed $default The value to be returned if variable doesn't exists
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     * @see \Kofenium\FilterData::normalize() Filters list
     */
    protected static function getVariable($uid = null, $type, $default = null, $filter = null)
    {
        if (empty($uid)) {
            if ($type === 'all') {
                $data = [];

                // reversing order
                for ($i = count(self::$arrayOrder) - 1; $i >= 0; $i--) {
                    $data = array_merge($data, self::${self::$arrayOrder[$i]});
                }
            } else {
                $data = self::${$type};
            }

            return $data;
        }

        if (!self::hasVariable($uid, $type)) {
            return $default;
        }

        if ($type === 'all') {
            $data = self::${self::$foundAt}[$uid];
        } else {
            $data = self::${$type}[$uid];
        }

        return ($filter !== null)
            ? FilterData::normalize($data, $filter)
            : $data;
    }
}
