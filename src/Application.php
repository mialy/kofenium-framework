<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use PDO;
use Exception;
use Kofenium\Input;
use Kofenium\Config;
use Kofenium\Singleton;
use Kofenium\Views\View;
use Kofenium\FrontController;
use Kofenium\Database\Database;
use Kofenium\Session\NativeSession;
use Kofenium\Router\RouterInterface;
use Kofenium\Session\DatabaseSession;
use Kofenium\Session\SessionInterface;

/**
 * Framework bootloader and main entry point
 *
 */
final class Application extends Singleton
{
    /**
     * Instance of \Kofenium\Config
     *
     * @var \Kofenium\Config
     */
    protected $config = null;

    /**
     * List of database connection instances
     *
     * @var array
     */
    protected $databasePool = [];

    /**
     * Current session instance
     *
     * @var \Kofenium\Session\SessionInterface
     */
    protected $session = null;

    /**
     * The front controller
     *
     * @var \Kofenium\FrontController
     */
    protected $frontController = null;

    /**
     * Router instance to handle controllers
     *
     * @var \RouterInterface
     */
    protected $router = null;

    /**
     * Base site URL
     *
     * @var string
     */
    protected $baseUrl = '';

    /**
     * List of class aliases
     *
     * @var array
     */
    protected $classAliases = [];

    /**
     * Constructor
     */
    protected function __construct()
    {
        set_exception_handler(array($this, 'exceptionHandler'));
        $this->config = Config::getInstance();
        if ($this->config->isPathUnset()) {
            $this->config->addPath(defined('CONFIG') ? CONFIG : '../config/');
        }

        Input::setAll([
            'get' => $_GET,
            'post' => $_POST,
            'server' => $_SERVER,
            'env' => $_ENV,
            'cookies' => $_COOKIE,
            'files' => $_FILES,
        ]);
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        if ($this->session instanceof SessionInterface) {
            $this->session->save();
        }
    }

    /**
     * Run the application
     */
    public function run()
    {
        $this->initClassAliases();
        $this->initFrontController();
        $this->initDatabases();
        $this->initSession();
        $this->initI18n();
        $this->frontController->dispatch($this->config->get('app.default_route', null));
    }

    /**
     * Auto connect to all database connections where 'autostart' key is true
     *
     */
    protected function initDatabases()
    {
        foreach ($this->config->database['connections'] as $name => $connection) {
            if (!empty($connection['autostart'])) {
                $this->getDatabaseConnection($name);
            }
        }
    }

    /**
     * Initialize default session
     *
     * @param bool $autostartOnly Create session only if the session.autostart is also true
     */
    protected function initSession($autostartOnly = true)
    {
        $session = $this->config->find('session', true);
        if (!$session || ($autostartOnly && empty($session['autostart']))) {
            return;
        }

        switch ($session['driver']) {
            case 'native':
                $this->session = new NativeSession($session['name'], $session);
                break;

            case 'database':
                $this->session = new DatabaseSession(
                    $session['connection'],
                    $session['table'],
                    $session['name'],
                    $session
                );
                break;

            case 'custom':
                if (!isset($session['custom']) || empty($session['custom'])) {
                    throw new Exception('Custom session class name not provided.', 500);
                } elseif (!class_exists($session['custom'])) {
                    throw new Exception('Custom session class cannot be loaded.', 500);
                }

                $this->setSession(new $session['custom']());
                break;

            default:
        }
    }

    /**
     * Init Internationalization
     *
     * TODO: add localizations
     */
    protected function initI18n()
    {
        date_default_timezone_set($this->config->get('app.timezone', 'UTC', 'trim'));

        // Setup UTF-8 to all mb_* functions
        extension_loaded('mbstring')
            && mb_internal_encoding('UTF-8')
            && mb_regex_encoding('UTF-8');
    }

    /**
     * Init the Front controller
     */
    protected function initFrontController()
    {
        $this->frontController = FrontController::getInstance();

        // load routes
        $this->config->find("routes");
        $this->frontController->setRouter($this->getRouter());
        $this->frontController->setRequest(urldecode(Input::server('REQUEST_URI')));
        $this->frontController->setRequestMethod(Input::server('REQUEST_METHOD'));
    }

    /**
     * Init shorthand class names
     * i.e. ClassName => \Vendor\Namespace\AnotherNamespace\ClassName
     *
     * Typical, for views, ClassName can be called directly without any namespaces
     * or root slash.
     *
     * For other places, where a "namespace"-keyword is used, the developer must
     * either add "use ClassName;" and use the class directly, or to add root slash
     * prefix, i.e. \ClassName;
     */
    protected function initClassAliases()
    {
        $aliases = $this->config->find('app.aliases', true);
        if ($aliases === null || !is_array($aliases) || !count($aliases)) {
            return;
        }

        foreach ($aliases as $alias => $original) {
            $this->classAliases[$alias] = $original;
        }

        // register loader before all others
        spl_autoload_register([$this, 'classAliasesLoader'], true, true);
    }

    /**
     * Add class alias before other loaders
     *
     * @param string $alias Class name
     */
    protected function classAliasesLoader($alias)
    {
        if (isset($this->classAliases[$alias])) {
            class_alias($this->classAliases[$alias], $alias);
        }
    }

    /**
     * Set session type
     *
     * @param object $session Session instance
     * @return \Kofenium\Application
     */
    public function setSession(SessionInterface $session)
    {
        $this->session = $session;
        return $this;
    }

    /**
     * Get Session
     *
     * @return \Kofenium\Session\SessionInterface Current session
     */
    public function getSession()
    {
        if (!$this->session) {
            $this->initSession(false);
        }
        return $this->session;
    }

    /**
     * Get config
     *
     * @return \Kofenium\Config Current instance
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Get Router instance
     *
     * @return \RouterInterface
     */
    public function getRouter()
    {
        if (!$this->router) {
            $router = $this->config->get('app.router', '\\Kofenium\\Router\\DefaultRouter');
            $this->router = new $router;
        }

        return $this->router;
    }

    /**
     * Get Database (PDO) connection instance
     *
     * @param string $name The name of the connection
     * @return \Kofenium\Database\Database
     * @throws Exception If $name not found in database.connections list
     */
    public function getDatabaseConnection($name = null)
    {
        if (empty($name) || !is_string($name)) {
            $name = $this->config->get('database.default');
        }

        if (isset($this->databasePool[$name])) {
            return $this->databasePool[$name];
        }

        $connection = $this->config->find('database.connections.' . $name, true);
        if (!is_array($connection) || !count($connection)) {
            throw new Exception("Invalid database connection name provided: " . $name);
        }

        $dsn = $this->buildPdoDsn($connection);
        $user = $this->config->get('database.connections.' . $name . '.username', '');
        $password = $this->config->get('database.connections.' . $name . '.password', '');
        $options = $this->config->get('database.options', [])
            + $this->config->get('database.connections.'.$name.'.options', []);

        $this->databasePool[$name] = new PDO($dsn, $user, $password, $options);

        return $this->databasePool[$name];
    }

    /**
     * Build PDO's DSN for specific driver/vendor
     *
     * Supported drivers are:
     * <ul>
     *  <li><b>mysql</b> - for MySQL/MariaDB</li>
     *  <li><b>sqlite</b> - for SQLite</li>
     *  <li><b>sqlsrv</b> - for SQL Server</li>
     *  <li><b>pgsql</b> - for PostgreSQL</li>
     * </ul>
     *
     * @param array $connection Connection array
     * @return string
     */
    private function buildPdoDsn($connection)
    {
        switch (strtolower($connection['driver'])) {
            case 'mysql':
                return 'mysql:' . http_build_query(array_filter([
                    'host' => !isset($connection['host']) ? null : $connection['host'],
                    'port' => !isset($connection['port']) ? null : (int) $connection['port'],
                    'dbname' => !isset($connection['database']) ? null : $connection['database'],
                    'charset' => !isset($connection['charset']) ? null : $connection['charset'],
                    'connect_timeout' => !isset($connection['timeout']) ? null : (int) $connection['timeout'],
                ]), '', ';');

            case 'sqlite':
                return 'sqlite:' . implode('', array_filter([
                    !isset($connection['database']) ? null : $connection['database'],
                ]));

            case 'pgsql':
                return 'pgsql:' . http_build_query(array_filter([
                    'host' => !isset($connection['host']) ? null : $connection['host'],
                    'port' => !isset($connection['port']) ? null : (int) $connection['port'],
                    'dbname' => !isset($connection['database']) ? null : $connection['database'],
                ]), '', ';');

            case 'sqlsrv':
                return 'sqlsrv:' . http_build_query(array_filter([
                    'Server' => (!isset($connection['host']) ? null : $connection['host'])
                        . (!isset($connection['port']) ? null : ',' . (int) $connection['port']),
                    'Database' => !isset($connection['database']) ? null : $connection['database'],
                    'LoginTimeout' => !isset($connection['timeout']) ? null : (int) $connection['timeout'],
                ]), '', ';');
            default:
                return '';
        }
    }

    /**
     * Get base URL
     *
     * @return string
     */
    public function getBaseUrl()
    {
        if ($this->baseUrl !== '') {
            return $this->baseUrl;
        }

        $secure = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on';
        $scheme = $secure ? 'https://' : 'http://';
        $domain = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'localhost';
        $port = (int) $_SERVER['SERVER_PORT'];
        $port = ($secure && $port !== 443) || (!$secure && $port !== 80) ? ':' . $port : '';

        // BaseUrl w/o path
        $this->baseUrl = $scheme . $domain . $port;

        // Get root path, by determine the htdocs location
        $htdocs = realpath($_SERVER['DOCUMENT_ROOT']);
        $scriptName = realpath($_SERVER['SCRIPT_FILENAME']);

        if (!is_dir($scriptName)) {
            $scriptName = dirname($scriptName);
        }

        if ($scriptName === $htdocs) {
            $this->baseUrl .= '/';
        } elseif (mb_strpos($scriptName, $htdocs, 0, 'UTF-8') === 0) {
            $scriptName = mb_substr($scriptName, mb_strlen($htdocs, 'UTF-8'), null, 'UTF-8');
            $scriptName = preg_replace("/\\\/iu", '/', $scriptName);
            $scriptName = rtrim($scriptName, '/') . '/';
            $this->baseUrl .= $scriptName;
        }

        return $this->baseUrl;
    }

    /**
     * Get DOCUMENT_ROOT, aka HTDOCS directory
     *
     * @return string
     */
    public function getDocumentRoot()
    {
        return realpath($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR;
    }

    /**
     * Handle all non-handled already exceptions within application
     *
     * @param mixed $e Exception or Throwable
     */
    public function exceptionHandler($e)
    {
        $displayErrors = false;
        try {
            $displayErrors = $this->config && $this->config->get('app.debug', false, 'bool');
        } catch (Exception $ex) { }

        $this->displayError([
            'name' => get_class($e),
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
            'file' => $e->getFile() . ':' . $e->getLine(),
            'trace' => $e->getTrace(),
            'display_errors' => $displayErrors,
        ]);
    }

    /**
     * Display exception error
     *
     * @param array $data Summary error from the exception
     * @see \Kofenium\Application::exceptionHandler
     */
    public function displayError($data)
    {
        $data['title'] = $data['display_errors']
            ? $data['message']
            : 'Not Found';

        $data['heading'] = $data['display_errors']
            ? 'Uncaught Exception: ' . $data['message']
            : 'Sorry, the page you are looking for could not be found.';

        foreach ($data['trace'] as $i => $info) {
            $args = [];
            if (!empty($info['args'])) {
                foreach ((array) $info['args'] as $arg) {
                    $args[] = is_array($arg) ? (count($arg) ? '[..]' : '[]')
                        : (is_object($arg) ? get_class($arg)
                        : (is_null($arg) ? 'null'
                        : (is_bool($arg) ? ($arg ? 'true' : 'false')
                        : (is_string($arg) ?
                            (mb_strlen($arg) <= 32
                                ? '"' . ((string) $arg) . '"'
                                : '"' . mb_substr((string) $arg, 0, 32) . '..."')
                        : (string) $arg))));
                }
            }

            $data['trace'][$i]['args_list'] = implode(', ', $args);
        }

        if (defined('STDIN')) {
            echo $data['heading'], "\n\n";
            if (!$data['display_errors']) {
                exit;
            }

            $nr = 0;
            $width = strlen(1 + count($data['trace']));
            echo sprintf(' %' . $width . 'd. ', ++$nr);
            echo $data['name'], '(', $data['message'], ')';
            echo ' in ', $data['file'], "\n";

            $traces = '';
            foreach ($data['trace'] as $i => $info) {
                echo sprintf(' %' . $width . 'd. ', ++$nr);
                echo 'at ';

                if (isset($info['class'])) {
                    echo $info['class'], $info['type'];
                }

                echo $info['function'], '(', $info['args_list'], ')';

                if (isset($info['file'])) {
                    echo ' in ', $info['file'], ':', $info['line'];
                }

                echo "\n";
            }
            exit;
        }

        try {
            $view = View::getInstance();
            $template = 'errors.default';

            if ($data['code'] > 0 && $view->isViewExists('errors.' . $data['code'])) {
                $template = 'errors.' . $data['code'];
            }

            echo (string) $view->with($data)->render($template)->output();
            exit;
        } catch (Exception $e) {
            // Something terribly wrong was happened
        }

        // fallback error
        $output = str_replace(
            ['{{title}}', '{{heading}}'],
            [$data['title'], $data['heading']],
            '<!DOCTYPE html><html lang="en">'
                . '<head><meta charset="utf-8" />'
                . '<title> {{title}} </title>'
                . '</head><body>'
                . '<h1> {{heading}} </h1>'
        );

        if (!$data['display_errors']) {
            $output .= '</body></html>';
            die($output);
        }

        $traces = '';
        foreach ($data['trace'] as $i => $info) {
            $traces .= str_replace(
                ['{{function}}', '{{args}}', '{{location}}', '{{file}}'],
                [
                    $info['function'],
                    htmlspecialchars($info['args_list'], ENT_COMPAT),
                    !isset($info['class']) ? '' : $info['class'] . $info['type'],
                    !isset($info['file']) ? '' : $info['file'].':'.$info['line']
                ],
                '<li>at {{location}}<strong>{{function}}'
                    . '(</strong><em> {{args}} </em><strong>)</strong>'
                    . (isset($info['file']) ? ' in <code>{{file}}</code>' : '')
                    . '</li>'
            );
        }

        $output .= str_replace(
            ['{{name}}', '{{message}}', '{{file}}', '{{traces}}'],
            [$data['name'], $data['message'], $data['file'], $traces],
            '<ol><li><strong> {{name}}("{{message}}")</strong> '
                .' in <code> {{file}} </code></li>'
                . '{{traces}}'
                . '</ol></body></html>'
        );

        echo $output;
        exit;
    }
}
