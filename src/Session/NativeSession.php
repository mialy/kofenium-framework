<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Session;

use InvalidArgumentException;
use Kofenium\Session\SessionInterface;

/**
 * Provide native sessions through PHP's session_* functions
 *
 */
class NativeSession implements SessionInterface
{
    /**
     * Session ID regeneration, in seconds
     *
     * @var integer
     */
    const REGENERATE_LIFETIME = 60;

    /**
     * Cookie attributes like path, domain, lifetime, etc.
     *
     * @var array
     */
    protected $cookie = [];

    /**
     * Init cookie and session parameters
     *
     * @param string $name Session name: an alphanumeric + underscore, at least 2 characters long string
     * @param array $cookie List of extended options for cookie
     * @see http://php.net/manual/en/session.configuration.php#ini.session.hash-function php.ini - session.hash_function
     * @see http://php.net/manual/en/session.configuration.php#ini.session.hash-bits-per-character php.ini - session.hash_bits_per_character
     */
    public function __construct($name, $cookie = [])
    {
        if (strlen($name) < 2) {
            $name = static::DEFAULT_SESSION_NAME;
        } elseif (preg_match('/[^a-z\d_]+/i', $name)) {
            throw new InvalidArgumentException('Invalid session name. Alpha-numeric session required, provided: ' . $name);
        }

        $this->cookie = array_merge([
            'lifetime' => static::DEFAULT_SESSION_LIFETIME,
            'path' => '/',
            'domain' => null,
            'secure' => false,
            'regenerate_lifetime' => static::DEFAULT_REGENERATE_LIFETIME,
        ], $cookie);

        // Increase SID length
        ini_set('session.hash_function', 'sha256');
        ini_set('session.hash_bits_per_character', 6);

        // init the session
        session_name($name);
        session_set_cookie_params(
            $this->cookie['lifetime'],
            $this->cookie['path'],
            $this->cookie['domain'],
            $this->cookie['secure'],
            true
        );

        // suppress errors
        set_error_handler(function() { });

        if (!session_start()) {
            session_regenerate_id(true);
        }

        if (isset($_COOKIE[$name]) && preg_match('/[^a-zA-Z0-9,\-]/', $_COOKIE[$name])) {
            session_regenerate_id(true);
        }

        restore_error_handler();
        $this->updateLifetime();

        if (empty($this->timeout) || $this->timeout <= time()) {
            if (isset($this->timeout)) {
                session_regenerate_id(true);
            }
            $this->timeout = time() + $this->cookie['regenerate_lifetime'];
        }
    }

    /**
     * Get Session unique ID
     *
     * @return string
     */
    public function getId()
    {
        return session_id();
    }

    /**
     * Save current session
     */
    public function save()
    {
        session_write_close();
    }

    /**
     * Destroy current session
     *
     * @return bool True on successfull, false - otherwise
     */
    public function destroy()
    {
        $params = session_get_cookie_params();
        setcookie(
            session_name(),
            '',
            time() - 42000,
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httponly']
        );
        return session_destroy();
    }

    /**
     * Get Variable from the session
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    /**
     * Set Variable to the session
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * Determine if dynamic property is set or empty
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name) {
        return isset($_SESSION[$name]);
    }

    /**
     * Update cookie lifetime
     */
    protected function updateLifetime()
    {
        // update lifetime
        setcookie(
            session_name(),
            $this->getId(),
            !$this->cookie['lifetime'] ? 0 : time() + $this->cookie['lifetime'],
            $this->cookie['path'],
            $this->cookie['domain'],
            $this->cookie['secure'],
            true
        );
    }
}
