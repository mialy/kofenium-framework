<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Session;

/**
 * Interface for all session classes
 */
interface SessionInterface
{
    /**
     * Default cookie's name
     *
     * @var string
     */
    const DEFAULT_SESSION_NAME = '_session';

    /**
     * Session lifetime, in seconds
     *
     * @var integer
     */
    const DEFAULT_SESSION_LIFETIME = 3600;

    /**
     * Session ID regeneration, in seconds
     *
     * @var integer
     */
    const DEFAULT_REGENERATE_LIFETIME = 300;

    /**
     * Get Session unique ID
     *
     * @return string
     */
    public function getId();

    /**
     * Save current session
     */
    public function save();

    /**
     * Destroy current session
     */
    public function destroy();

    /**
     * Get Variable from the session
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name);

    /**
     * Set Variable to the session
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value);
}
