<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Session;

use InvalidArgumentException;
use Kofenium\Database\Database;
use Kofenium\Session\SessionInterface;

/**
 * Database-based sessions through PDO wrapper
 */
class DatabaseSession extends Database implements SessionInterface
{
    /**
     * Minimum Session ID length
     *
     * @var integer
     */
    const MIN_SID_LENGTH = 43;

    /**
     * Cookie attributes like path, domain, lifetime, etc.
     *
     * @var array
     */
    protected $cookie = [];

    /**
     * Database table name
     *
     * @var string
     */
    protected $table = '';

    /**
     * Session name
     *
     * @var string
     */
    protected $name = '';

    /**
     * Session ID
     *
     * @var string
     */
    protected $sid = '';

    /**
     * Session Data
     *
     * @var array
     */
    protected $data = [];

    /**
     * Init cookie and session parameters
     *
     * @param mixed $dbConnection An PDO instance, or connection name from database.connections
     * @param string $tableName Database's table name where the sessions will be saved
     * @param string $name Session name: an alphanumeric + underscore, at least 2 characters long string
     * @param array $cookie List of extended options for cookie
     * @see \Kofenium\Database\Database::__construct() \Database constructor
     */
    public function __construct($dbConnection, $tableName, $name, $cookie = [])
    {
        parent::__construct($dbConnection);

        if (strlen($name) < 2) {
            $name = static::DEFAULT_SESSION_NAME;
        } elseif (preg_match('/[^a-z\d_]+/i', $name)) {
            throw new InvalidArgumentException('Invalid session name. Alpha-numeric session required, provided: ' . $name);
        }

        $this->table = $tableName;
        $this->cookie = array_merge([
            'lifetime' => static::DEFAULT_SESSION_LIFETIME,
            'path' => '/',
            'domain' => null,
            'secure' => false,
            'regenerate_lifetime' => static::DEFAULT_REGENERATE_LIFETIME,
        ], $cookie);

        $this->name = $name;
        $this->sid = isset($_COOKIE[$name]) ? $_COOKIE[$name] : '';

        if (strlen($this->sid) <= self::MIN_SID_LENGTH || !$this->isSessionValid()) {
            $this->generateSid();
        }

        $this->deleteExpiredSessions();
        $this->updateLifetime();

        if (empty($this->timeout) || $this->timeout <= time()) {
            if (isset($this->timeout)) {
                $this->regenerateSid();
            }
            $this->timeout = time() + $this->cookie['regenerate_lifetime'];
        }
    }

    /**
     * Get Session unique sid
     *
     * @return string
     */
    public function getId()
    {
        return $this->sid;
    }

    /**
     * Save current session
     */
    public function save()
    {
        if (!$this->sid) {
            return;
        }

        $sql = "UPDATE ". $this->table . "
                SET
                    modified = :modified,
                    data = :data
                WHERE
                    sid = :sid
                LIMIT 1";
        $this->prepare($sql)->execute([
            'sid' => $this->sid,
            'modified' => date("Y-m-d H:i:s"),
            'data' => $this->encodeData($this->data),
        ]);
    }

    /**
     * Destroy current session
     *
     * @return bool True on successfull, false - otherwise
     */
    public function destroy()
    {
        $params = session_get_cookie_params();
        setcookie(
            $this->name,
            '',
            time() - 42000,
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httponly']
        );

        $sql = "DELETE FROM " . $this->table . "
                WHERE sid = :sid";
        $this->prepare($sql, ['sid' => $this->sid])->execute();
        $this->sid = '';
    }

    /**
     * Get Variable from the session
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    /**
     * Set Variable to the session
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * Determine if dynamic property is set or empty
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * Set the session cookie
     */
    protected function setCookie()
    {
        setcookie(
            $this->name,
            $this->sid,
            !$this->cookie['lifetime'] ? 0 : time() + $this->cookie['lifetime'],
            $this->cookie['path'],
            $this->cookie['domain'],
            $this->cookie['secure'],
            true
        );
    }

    /**
     * Update cookie lifetime
     */
    protected function updateLifetime()
    {
        $this->setCookie();

        $sql = "UPDATE " . $this->table . "
                SET
                    modified = :modified,
                    valid_until = :valid_until
                WHERE
                    sid = :sid
                LIMIT 1";
        $this->prepare($sql)->execute([
            'sid' => $this->sid,
            'modified' => date("Y-m-d H:i:s"),
            'valid_until' => $this->getValidUntilDate(),
        ]);
    }

    /**
     * Generate new session ID
     */
    protected function generateSid()
    {
        $this->sid = '';
        $attempts = 20;
        $isSessionGenerated = false;

        while (!$isSessionGenerated && $attempts-- >= 0) {
            $this->sid = hash('sha256', mt_rand());
            $this->sid = base64_encode(pack('H*', $this->sid));

            $sql = "SELECT COUNT(*)
                    FROM " . $this->table . "
                    WHERE sid = :sid
                    LIMIT 1";
            $this->prepare($sql, ['sid' => $this->sid])->execute();
            if ($this->fetchColumn()) {
                continue;
            }

            $sql = "INSERT INTO " . $this->table . " (
                        sid,
                        data,
                        added,
                        modified,
                        valid_until
                    ) VALUES (
                        :sid,
                        :data,
                        :added,
                        :modified,
                        :valid_until
                    )";
            $this->prepare($sql)->execute([
                'sid' => $this->sid,
                'data' => $this->encodeData($this->data),
                'added' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                'valid_until' => $this->getValidUntilDate(),
            ]);

            $isSessionGenerated = true;
        }

        if (!$isSessionGenerated) {
            $this->sid = '';
            return;
        }

        $this->setCookie();
    }

    /**
     * Force session ID regeneration
     *
     */
    protected function regenerateSid()
    {
        $this->destroy();
        $this->generateSid();
    }

    /**
     * Remove expired sessions
     *
     * @param bool $forced Forced deletion
     */
    protected function deleteExpiredSessions($forced = false)
    {
        if (mt_rand(0, 40) !== 1 && !$forced) {
            return;
        }

        $sql = "DELETE FROM " . $this->table . "
                WHERE valid_until <= NOW()";
        $this->prepare($sql)->execute();
    }

    /**
     * Check if session is valid and get data array
     *
     * @return bool True if session is valid
     */
    protected function isSessionValid()
    {
        if (!$this->sid) {
            return false;
        }

        $sql = "SELECT data
                FROM " . $this->table . "
                WHERE sid = :sid AND valid_until > NOW()
                LIMIT 1";
        $result = $this->prepare($sql, ['sid' => $this->sid])->execute()->fetchAssoc();
        if ($result) {
            $this->data = $this->decodeData($result['data']);
            return true;
        }

        return false;
    }

    /**
     * Encode data
     *
     * @param array $data Data array to be encoded for the database
     * @return string
     */
    protected function encodeData($data)
    {
        return json_encode($data);
    }

    /**
     * Decode Data
     *
     * @param string $data String from database to be decoded into array
     * @return array
     */
    protected function decodeData($data)
    {
        return json_decode($data, true);
    }

    /**
     * Get (next) Valid Datetime for session expiration
     *
     * @return string
     */
    protected function getValidUntilDate()
    {
        return date("Y-m-d H:i:s", time() + $this->cookie['lifetime']);
    }
}
