<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Router;

use AltoRouter;
use Kofenium\Router\RouterMatch;
use Kofenium\Router\RouterInterface;

/**
 * Default router used in Kofenium
 */
class DefaultRouter implements RouterInterface {
    protected $router;
    protected $supportedMethods = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'OPTIONS',
        'PATCH',
        'HEAD',
    ];

    /**
     * Class constructor
     */
    public function __construct() {
        $this->router = new AltoRouter();
    }

    /**
     * Get additional data of the match route
     *
     * @param string $url Requested URL
     * @param string $method POST, GET, etc.
     * @return \RouterMatch|null
     */
    public function match($url, $method) {
        return $this->router->match($url, $method);
    }

    public function map($method, $route, $target, $name = null) {
        $method = is_array($method)
            ? implode('|', array_map('strtoupper', $method))
            : strtoupper($method);
        return $this->router->map(mb_strtoupper($method), $route, $target, $name);
    }

    /**
     * Mapping a new route as GET HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function get($route, $target, $name = null) {
        return $this->map('GET', $route, $target, $name);
    }

    /**
     * Mapping a new route as POST HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function post($route, $target, $name = null) {
        return $this->map('POST', $route, $target, $name);
    }

    /**
     * Mapping a new route as DELETE HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function delete($route, $target, $name = null) {
        return $this->map('DELETE', $route, $target, $name);
    }

    /**
     * Mapping a new route as PUT HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function put($route, $target, $name = null) {
        return $this->map('PUT', $route, $target, $name);
    }

    /**
     * Mapping a new route as OPTIONS HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function options($route, $target, $name = null) {
        return $this->map('OPTIONS', $route, $target, $name);
    }

    /**
     * Mapping a new route as PATCH HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function patch($route, $target, $name = null) {
        return $this->map('PATCH', $route, $target, $name);
    }

    /**
     * Mapping a new route as HEAD HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function head($route, $target, $name = null) {
        return $this->map('HEAD', $route, $target, $name);
    }

    /**
     * Mapping a new route as Any (i.e. all possible methods)
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function any($route, $target, $name = null) {
        return $this->map($this->$supportedMethods, $route, $target, $name);
    }

    /**
     * Generate URL from routername and params
     *
     * @param string $routerName Name of the route, defined on router mapping
     * @param mixed $params Associative array of all required params for the router
     * @return string
     */
    public function generate($routerName, $params) {
        return $this->router->generate($routerName, $params);
    }
};
