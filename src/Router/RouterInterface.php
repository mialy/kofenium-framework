<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Router;

use Kofenium\Router\RouterTarget;

/**
 * Contract for all routes, that can be used inside Kofenium
 */
interface RouterInterface {
    /**
     * Generate URL from routername and params
     *
     * @param string $routerName Name of the route, defined on router mapping
     * @param mixed $params Associative array of all required params for the router
     * @return string
     */
    public function generate($routerName, $params);

    /**
     * Get additional data of the match route
     *
     * @param string $url Requested URL
     * @param string $method POST, GET, etc.
     * @return \RouterTarget|null
     */
    public function match($url, $method);

    /**
     * Mapping a new route
     *
     * @param string|array $method An HTTP method, i.e. GET, POST, PATCH, PUT, DELETE, HEAD, OPTIONS; case insensitive
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function map($method, $route, $target, $name = null);

    /**
     * Mapping a new route as Any (i.e. all possible methods)
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function any($route, $target, $name = null);

    /**
     * Mapping a new route as GET HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function get($route, $target, $name = null);

    /**
     * Mapping a new route as POST HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function post($route, $target, $name = null);

    /**
     * Mapping a new route as DELETE HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function delete($route, $target, $name = null);

    /**
     * Mapping a new route as PATCH HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function patch($route, $target, $name = null);

    /**
     * Mapping a new route as PUT HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function put($route, $target, $name = null);

    /**
     * Mapping a new route as HEAD HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function head($route, $target, $name = null);

    /**
     * Mapping a new route as OPTIONS HTTP method
     *
     * @param string $route Define a route; This include specific for the route match syntax
     * @param mixed $target What to be executed, i.e. function name, additional info, etc.
     * @param string $name Naming of the this target
     */
    public function options($route, $target, $name = null);

}
