<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Views;

use Exception;
use Kofenium\Config;
use Kofenium\Singleton;
use Kofenium\FilterData;
use BadMethodCallException;
use Kofenium\Views\ViewsAdapterInterface;

/**
 * Class for View templates (from MVC pattern)
 */
class View extends Singleton
{
    /**
     * Instance of the Config object
     *
     * @var \Kofenium\Config
     */
    protected $config = null;

    /**
     * Where to start search for templates
     *
     * @var string
     */
    protected $path = '';

    /**
     * List of registered engines
     *
     * @var array
     */
    protected $engines = [];

    /**
     * List of extensions used by registered engines
     *
     * @var array
     */
    protected $extensions = [];

    /**
     * Selected engine for all templates, regardless of the template's extension
     *
     * @var string
     */
    protected $forcedEngine = null;

    /**
     * Selected engine
     *
     */
    protected $engine = '';

    /**
     * Selected template
     *
     * @var string
     */
    protected $view = '';

    /**
     * List of variables to the templates
     *
     * @var array
     */
    protected $data = [];

    /**
     * Fallback view list, if there is no any view engines to register for
     *
     * @var array
     */
    protected $fallbackViews = [];

    /**
     * Fallback extensions list
     *
     * @var array
     */
    protected $fallbackExtensions = [];

    /**
     * Setup the View
     */
    protected function __construct()
    {

        // setup fallback class if there is no any found in app.views.* config
        $this->fallbackViews = [
            'php' => '\\Kofenium\\Views\\PhpAdapter',
        ];
        $this->fallbackExtensions = [
            'php' => 'php',
        ];

        $this->config = Config::getInstance();
        $this->setPath($this->config->get('app.views.path', '../app/Views/'));
        $this->registerViews();
    }

    /**
     * Register all view engines at once
     *
     * @throws \Exception on invalid adapter class
     */
    protected function registerViews()
    {
        $list = (array) $this->config->find('app.views.engines', true);

        // add default PHP view as
        if (!count($list)) {
            $list = $this->fallbackViews;
        }

        foreach ($list as $engine => $adapterClass) {
            if (!class_exists($adapterClass, true)) {
                throw new Exception('The Provided adapter class is not exists: ' . $adapterClass);
            }

            $adapter = new $adapterClass();

            if (!$adapter instanceof ViewsAdapterInterface) {
                throw new Exception('The Provided adapter class not implement the ViewsAdapterInterface: ' . $adapterClass);
            }

            $this->engines[$engine] = $adapter->init();
        }

        // add file extensions to only registered views
        $extensions = (array) $this->config->find('app.views.extensions', true);

        // add default PHP extension
        if (!count($extensions)) {
            $extensions = $this->fallbackExtensions;
        }

        foreach ($extensions as $extension => $engine) {
            if (!isset($this->engines[$engine])) {
                continue;
            }

            $this->extensions[$extension] = $engine;
        }
    }

    /**
     * Get path to the view and view name itself (without extension), plus additional dot
     *
     * @param string $name The template name
     * @return string
     */
    protected function getViewPath($name)
    {
        // normalize the view template
        $view = str_replace('.', DIRECTORY_SEPARATOR, $name);
        $view = realpath($this->path) . DIRECTORY_SEPARATOR . $view . '.';
        return $view;
    }

    /**
     * Gets filename of the current view
     *
     * @param null|string $name The template name
     * @param null|string $forcedEngine Specify engine
     * @return bool|string False on not found, otherwise - filename of the view, relative to View's path
     */
    public function searchView($name = null, $forcedEngine = null)
    {
        $name = $name ?: $this->view;
        $forcedEngine = $forcedEngine ?: $this->forcedEngine;

        $view = $this->getViewPath($name);

        clearstatcache();

        $foundView = false;
        $this->engine = '';

        foreach ($this->extensions as $ext => $engine) {
            if ($forcedEngine && $engine !== $forcedEngine) {
                continue;
            }

            $filename = $view . $ext;

            if (!is_file($filename) || !is_readable($filename)) {
                continue;
            }

            $path = realpath($this->path) . DIRECTORY_SEPARATOR;
            $foundView = mb_substr($filename, mb_strlen($path));

            $this->engine = $this->engines[$engine];
            break;
        }

        return $foundView;
    }

    /**
     * Check if the view exists
     *
     * @param string $name The template file.
     * @param null|string $forcedEngine Specify engine
     */
    public function isViewExists($name, $forcedEngine = null)
    {
        $view = $this->getViewPath($name);

        clearstatcache();

        foreach ($this->extensions as $ext => $engine) {
            if ($forcedEngine && $engine !== $forcedEngine) {
                continue;
            }

            $filename = $view . $ext;

            if (is_file($filename) && is_readable($filename)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Force engine - Force the engine to be used as renderer, instead of relay
     * on extension's order.
     *
     * @param string|null $name Engine name to be set
     * @return View
     */
    public function setEngine($name = null)
    {
        $this->forcedEngine = $name;
        return $this;
    }

    /**
     * Get current forced engine
     *
     * @param string|null $name Engine name to be set
     * @return View
     */
    public function getEngine()
    {
        return $this->forcedEngine;
    }

    /**
     * Reset
     */
    public function defaultEngine()
    {
        return $this->setEngine(null);
    }

    /**
     * Set path for views
     *
     * @param string $path Path to views
     * @return View
     * @throws Exception
     */
    public function setPath($path)
    {
        $this->path = trim($path);
        $this->path = rtrim($path, '/\\') . '/';
        return $this;
    }

    /**
     * Choose the template to render
     *
     * @param string $template The template name. The name is in dot-notation,
     *    represent subdirectories. File extension must omitted.
     * @param array $data Key-Value array passed to the template
     * @return View
     */
    public function render($name, $data = [])
    {
        $this->view = $name;
        $this->with($data);
        return $this;
    }

    /**
     * Return the rendered template as string
     *
     * @return string
     * @throws \Exception on not set, or not found template
     */
    public function output()
    {
        if (!$this->view) {
            throw new Exception("No selected view!");
        }

        $view = $this->searchView();
        if (!$view) {
            throw New Exception("The View was not found: " . $this->view);
        }

        return $this->engine
            ->set($this->data)
            ->render($view);
    }

    /**
     * Return the rendered template as string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->output();
    }

    /**
     * Get variable from the view
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    /**
     * Set variable to the view
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * Set variable to the view through view* methods,
     * i.e. viewKeyNameMethod('value') will convert to view('key_name_method', 'value')
     *
     * @param string $name Key name as CamelCase
     * @param array $params Value for the key
     * @return View
     * @throws \BadMethodCallException If the method doesn't starts with 'with' name
     */
    public function __call($name, $params)
    {
        if (strpos($name, 'with') === 0) {
            $name = FilterData::toSnakeCase(substr($name, 4));
            return $this->with($name, $params[0]);
        }

        throw new BadMethodCallException("The called method doesn't exist: " . $name);
    }

    /**
     * Check if set data variable
     *
     * @param string $name Name of the variable
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * Set variables to the view
     *
     * @param mixed $key String or key-value Array
     * @param mixed $value If $key is a string, this is the value
     * @return View
     */
    public function with($key = null, $value = null)
    {
        if ($key === null) {
            return $this;
        } elseif (is_string($key)) {
            $this->data[$key] = $value;
        } elseif (is_array($key)) {
            $this->data = array_merge($this->data, $key);
        }

        return $this;
    }
}
