<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Views;

/**
 * Interface for all view adapters
 */
interface ViewsAdapterInterface
{
    /**
     * Initialize the class
     *
     * @return Kofenium\Views\ViewsAdapterInterface
     */
    public function init();

    /**
     * Set variables to the view
     *
     * @param mixed $key String or key-value Array
     * @param mixed $value If $key is a string, this is the value
     * @return Kofenium\Views\ViewsAdapterInterface
     */
    public function set($key = null, $value = null);

    /**
     * Get rendered template as HTML string
     *
     * @param string $filename Filename name of the template
     * @return string
     */
    public function render($filename);
}
