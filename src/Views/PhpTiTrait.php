<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Views;

use Kofenium\Views\View;

/**
 * Methods for inheritance, based on PHP Template Inheritance v0.9
 * by Adam Shaw (http://arshaw.com/), licensed under the MIT License
 *
 * @url http://arshaw.com/phpti/
 */
trait PhpTiTrait
{
    /**
     * The array contains children blocks
     *
     * @var array
     */
    private $tiBase = null;

    /**
     * List of open blocks
     *
     * @var array
     */
    private $tiStack = [];

    /**
     * Number of nested levels in output buffer mechanism (ob_*)
     *
     * @var int
     */
    private $tiLevel = 0;

    /**
     * The array contains the all blocks data
     *
     * @var array
     */
    private $tiHash = [];

    /**
     * Number of the chars from output buffer to be cut before outputing
     *
     * @var int
     */
    private $tiEnd = null;

    /**
     * The string after all blocks
     *
     * @var string
     */
    private $tiAfter = '';

    /**
     * Starts a new block section within templates
     *
     * @param string $name The name of the section
     */
    public function blockStart($name)
    {
        $trace = $this->tiCallingTrace();
        $this->tiInit($trace);
        $this->tiStack[] = $this->tiNewBlock($name, $trace);
    }

    /**
     * Shorthand for calling blockStart() and blockEnd()
     *
     * @param string $name The name of the section
     */
    public function blockEmpty($name)
    {
        $trace = $this->tiCallingTrace();
        $this->tiInit($trace);
        $this->tiInsertBlock($this->tiNewBlock($name, $trace));
    }

    /**
     * Ends current block
     *
     * @param string $name Optionally name of the block.
     */
    public function blockEnd($name = null)
    {
        $trace = $this->tiCallingTrace();
        $this->tiInit($trace);

        if ($this->tiStack) {
            $block = array_pop($this->tiStack);
            if ($name && $name != $block['name']) {
                $this->tiWarning("blockStart('{$block['name']}') does not match blockEnd('$name')", $trace);
            }
            $this->tiInsertBlock($block);
        } else {
            $this->tiWarning(
                $name
                    ? "Orphan blockEnd('$name')"
                    : "Orphan blockEnd()",
                $trace
            );
        }
    }

    /**
     * Flush current stack of blocks. Should be used right after included template
     * file and before the ob_end_clean() call.
     *
     * @url http://arshaw.com/phpti/#flushing-capturing
     */
    public function blocksFlush()
    {
        if (!$this->tiBase) {
            return;
        }
        while ($block = array_pop($this->tiStack)) {
            $this->tiWarning(
                "Missing blockEnd() for blockStart('{$block['name']}')",
                $this->tiCallingTrace(),
                $block['trace']
            );
        }

        while (ob_get_level() > $this->tiLevel) {
            ob_end_flush();
        }

        $this->tiBase = null;
        $this->tiStack = null;
    }

    /**
     * Init blocks in parent's without any block section
     *
     * @url http://arshaw.com/phpti/#base-no-blocks
     */
    public function blockBase()
    {
        $this->tiInit($this->tiCallingTrace());
    }

    /**
     * Gets and print the block's parent
     *
     * @url http://arshaw.com/phpti/#super-block
     */
    public function blockSuper()
    {
        if ($this->tiStack) {
            echo $this->blockSuperAsString();
            return;
        }

        $this->tiWarning(
            "blockSuper() call must be within a block",
            $this->tiCallingTrace()
        );
    }

    /**
     * Gets the block's parent as string
     *
     * @return string
     * @see PhpTiTrait::blockSuper() Method blockSuper()
     */
    public function blockSuperAsString()
    {
        if ($this->tiStack) {
            $block = end($this->tiStack);

            if (!isset($this->tiHash[$block['name']])) {
                return '';
            }

            return implode(
                $this->tiCompile(
                    $this->tiHash[$block['name']]['block'],
                    ob_get_contents()
                )
            );

        } else {
            $this->tiWarning(
                "blockSuperAsString() call must be within a block",
                $this->tiCallingTrace()
            );
        }

        return '';
    }

    /**
     * Extend the template by using parent view
     *
     * @param string $name The template name.
     */
    public function extend($___name)
    {
        // send all variables to the next loaded template
        extract($this->data, EXTR_SKIP);

        // find out what template is this and loaded it
        include realpath($this->path) . '/' . View::getInstance()->searchView($___name);
    }

    /**
     * Callback for ob_start()
     *
     * @param string $buffer ob_start()'s Buffer
     * @return string
     */
    private function tiBufferCallback($buffer)
    {
        if (!$this->tiBase) {
            return '';
        }

        while ($block = array_pop($this->tiStack)) {
            $this->tiInsertBlock($block);
            $this->tiWarning(
                "Missing blockEnd() for blockStart('{$block['name']}')",
                $this->tiCallingTrace(),
                $block['trace']
            );
        }

        if ($this->tiBase['end'] === null) {
            $this->tiBase['end'] = mb_strlen($buffer);
            $this->tiEnd = null;
        }

        $parts = $this->tiCompile($this->tiBase, $buffer);

        $i = count($parts) - 1;
        $parts[$i] = rtrim($parts[$i]);

        if ($this->tiEnd !== null) {
            $parts[] = mb_substr($buffer, $this->tiEnd);
        }

        $parts[] = $this->tiAfter;
        return implode($parts);
    }

    /**
     * Initialize the TI' stack
     *
     * @param array $trace The debug_backtrace()'s output
     */
    private function tiInit($trace)
    {
        if ($this->tiBase && !$this->tiInBaseOrChild($trace)) {
            $this->blocksFlush();
        }

        if (!$this->tiBase) {
            $this->tiBase = [
                'trace' => $trace,
                'children' => [],
                'start' => 0,
                'end' => null
            ];

            $this->tiLevel = ob_get_level();
            $this->tiStack = [];
            $this->tiHash = [];
            $this->tiEnd = null;
            $this->tiAfter = '';

            ob_start([$this, 'tiBufferCallback']);
        }
    }

    /**
     * Get trace from debug_backtrace(), exluding this class
     *
     * @return array The debug_backtrace()'s output
     */
    private function tiCallingTrace()
    {
        $trace = debug_backtrace();
        foreach ($trace as $i => $location) {
            if ($location['file'] !== __FILE__) {
                return array_slice($trace, $i);
            }
        }
    }

    /**
     * Determine if the trace is in base or child trace
     *
     * @param array $trace The debug_backtrace()'s output
     * @return bool True if it is, otherwise - false
     */
    private function tiInBaseOrChild($trace)
    {
        $baseTrace = $this->tiBase['trace'];

        return $trace
            && $baseTrace
            && $this->tiIsSubtrace(array_slice($trace, 1), $baseTrace)
            && $trace[0]['file'] === $baseTrace[count($baseTrace) - count($trace)]['file'];
    }

    /**
     * Check if child is a subtrace of parent backtrace
     *
     * @param array $child Backtrace array
     * @param array $parent Backtrace array
     * @return bool True if it is
     */
    private function tiIsSubtrace($child, $parent)
    {
        $childLength = count($child);
        $parentLength = count($parent);

        if ($childLength > $parentLength) {
            return false;
        }

        for ($i = 0; $i < $childLength; $i++) {
            if ($child[$childLength - 1 - $i] !== $parent[$parentLength - 1 - $i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Compile current children blocks
     *
     * @param array $block
     * @param string $buffer
     * @return array
     */
    private function tiCompile($block, $buffer)
    {
        $parts = [];
        $previ = $block['start'];

        foreach ($block['children'] as $childAnchor) {
            $parts[] = mb_substr($buffer, $previ, $childAnchor['start'] - $previ);
            $parts = array_merge(
                $parts,
                $this->tiCompile($childAnchor['block'], $buffer)
            );
            $previ = $childAnchor['end'];
        }

        if ($previ != $block['end']) {
            $parts[] = mb_substr($buffer, $previ, $block['end'] - $previ);
        }

        return $parts;
    }

    /**
     * Show Warning message in either plain text or in HTML format.
     *
     * @param string $message Warning message
     * @param array $trace Array output from debug_backtrace()
     * @param array $warningTrace Array output from debug_backtrace()
     */
    private function tiWarning($message, $trace, $warningTrace = null)
    {
        if (error_reporting() & E_USER_WARNING) {
            if (defined('STDIN')) {
                $format = "\nWarning: %s in %s on line %d\n";
            } else {
                $format = "<br />\n<b>Warning</b>:  %s in <b>%s</b> on line <b>%d</b><br />\n";
            }

            if (!$warningTrace) {
                $warningTrace = $trace;
            }

            $s = sprintf($format, $message, $warningTrace[0]['file'], $warningTrace[0]['line']);

            if (!$this->tiBase || $this->tiInBase($trace)) {
                echo $s;
            } else {
                $this->tiAfter .= $s;
            }
        }
    }

    /**
     * Check if two backtraces are the same, i.e. they are from the same file
     *
     * @param array $trace1 The debug_backtrace()'s array from the first trace
     * @param array $trace2 The debug_backtrace()'s array from the second trace
     * @return bool True if they are the same, otherwise - false
     */
    private function tiIsSameFile($trace1, $trace2)
    {
        return $trace1 && $trace2
            && $trace1[0]['file'] === $trace2[0]['file']
            && array_slice($trace1, 1) === array_slice($trace2, 1);
    }

    /**
     * Check if trace is within base trace
     *
     * @param array $trace The debug_backtrace()'s output
     * @return bool
     */
    private function tiInBase($trace)
    {
        return $this->tiIsSameFile(
            $trace,
            isset($this->tiBase['trace']) ? $this->tiBase['trace'] : []
        );
    }

    /**
     * Create new block's data info
     *
     * @param string $name The name of the block
     * @param array $trace The debug_backtrace()'s output
     * @return array
     */
    private function tiNewBlock($name, $trace)
    {
        while ($block = end($this->tiStack)) {
            if ($this->tiIsSameFile($block['trace'], $trace)) {
                break;
            } else {
                array_pop($this->tiStack);
                $this->tiInsertBlock($block);
                $this->tiWarning(
                    "Missing blockEnd() for blockStart('{$block['name']}')",
                    $this->tiCallingTrace(),
                    $block['trace']
                );
            }
        }

        if ($this->tiBase['end'] === null && !$this->tiInBase($trace)) {
            $this->tiBase['end'] = ob_get_length();
        }

        return [
            'name' => $name,
            'trace' => $trace,
            'children' => [],
            'start' => ob_get_length()
        ];
    }

    /**
     * Insert block into stack
     *
     * @param array $block The block data
     */
    private function tiInsertBlock($block)
    {
        $block['end'] = $this->tiEnd = ob_get_length();
        $name = $block['name'];

        if ($this->tiStack || $this->tiInBase($block['trace'])) {
            $blockAnchor = [
                'start' => $block['start'],
                'end' => $this->tiEnd,
                'block' => $block
            ];

            if ($this->tiStack) {
                $this->tiStack[count($this->tiStack)-1]['children'][] =& $blockAnchor;
            } else {
                $this->tiBase['children'][] =& $blockAnchor;
            }

            $this->tiHash[$name] =& $blockAnchor;
        } elseif (isset($this->tiHash[$name])) {
            if ($this->tiIsSameFile($this->tiHash[$name]['block']['trace'], $block['trace'])) {
                $this->tiWarning(
                    "Cannot define another block called '$name'",
                    $this->tiCallingTrace(),
                    $block['trace']
                );
            } else {
                $this->tiHash[$name]['block'] = $block;
            }
        }
    }
}