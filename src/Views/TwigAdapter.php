<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Views;

use Kofenium\Application;
use Kofenium\Views\ViewsAdapterInterface;
// PSR-0
use Twig_Environment;
use Twig_Loader_Filesystem;

/**
 * Class adapter for Twig template engine
 *
 * @url http://twig.sensiolabs.org/
 */
class TwigAdapter implements ViewsAdapterInterface
{
    /**
     * Twig engine instance
     *
     * @var \Twig_Environment
     */
    protected $twig = null;

    /**
     * Templates' variables
     *
     * @var array
     */
    protected $data = [];

    /**
     * Initialization
     *
     * @return TwigAdapter
     */
    public function init()
    {
        if ($this->twig) {
            return $this;
        }

        $app = Application::getInstance();
        $cfg = $app->getConfig();

        $viewsPath = $cfg->find('app.views.path', '../app/Views/');
        $viewsPath = realpath($viewsPath[0]) . '/';

        $theLoader = new Twig_Loader_Filesystem($viewsPath);
        $this->twig = new Twig_Environment($theLoader, [
            'strict_variables' => false,
            'debug' => $cfg->get('app.debug', false),
            'cache' => $cfg->get('app.views.options.twig.compile_dir', false),
            'autoescape' => $cfg->get('app.views.options.twig.autoescape', 'html'),
            'charset' => $cfg->get('app.views.options.twig.charset', 'UTF-8', 'upper'),
            'auto_reload' => $cfg->get('app.views.options.twig.auto_reload', null),
        ]);

        return $this;
    }

    /**
     * Set variables to the view
     *
     * @param mixed $key String or key-value Array
     * @param mixed $value If $key is a string, this is the value
     * @return TwigAdapter
     */
    public function set($key = null, $value = null)
    {
        if (is_string($key)) {
            $this->data[$key] = $value;
        } elseif (is_array($key)) {
            $this->data = array_merge($this->data, $key);
        }

        return $this;
    }

    /**
     * Render the selected template
     *
     * @param string $filename Template filename, relative to the view's path
     * @return string
     */
    public function render($filename)
    {
        return $this->twig->render($filename, $this->data);
    }
}
