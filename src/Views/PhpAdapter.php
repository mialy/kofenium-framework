<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Views;

use Kofenium\Config;
use Kofenium\Views\PhpTiTrait;
use Kofenium\Views\ViewsAdapterInterface;

/**
 * Default PHP view adapter
 */
class PhpAdapter implements ViewsAdapterInterface
{
    use PhpTiTrait;

    /**
     * Templates' variables
     *
     * @var array
     */
    protected $data = [];

    /**
     * Root's path for all views
     *
     * @var string
     */
    protected $path = '';

    /**
     * Initialization
     *
     * @return PhpAdapter
     */
    public function init()
    {
        $path = Config::getInstance()->get('app.views.path', '../app/Views/');
        $path = trim($path);
        $path = rtrim($path, '/\\') . '/';
        $this->path = realpath($path) . DIRECTORY_SEPARATOR;
        return $this;
    }

    /**
     * Set variables to the view
     *
     * @param mixed $key String or key-value Array
     * @param mixed $value If $key is a string, this is the value
     * @return PhpAdapter
     */
    public function set($key = null, $value = null)
    {
        if (is_string($key)) {
            $this->data[$key] = $value;
        } elseif (is_array($key)) {
            $this->data = array_merge($this->data, $key);
        }

        return $this;
    }

    /**
     * Render the selected template
     *
     * @param string $filename Template filename, relative to the view's path
     * @return string
     */
    public function render($filename)
    {
        $errorReporting = ini_get("error_reporting");
        error_reporting($errorReporting & ~E_NOTICE);
        $out = $this->output($this->path . $filename, $this->data);
        error_reporting($errorReporting);
        return $out;
    }

    /**
     * Get template output file
     *
     * @param string $___file Full path to template file
     * @param mixed $___data Data send to template
     * @return string
     */
    protected function output($___file, $___data)
    {
        extract($___data, EXTR_SKIP);
        ob_start();
        include $___file;
        $this->blocksFlush();
        return ob_get_clean();
    }
}
