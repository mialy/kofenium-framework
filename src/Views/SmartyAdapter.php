<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Views;

use Kofenium\Application;
use Kofenium\Views\ViewsAdapterInterface;
// Classmap
use Smarty;

/**
 * Class adapter for Smarty PHP template engine
 *
 * @url http://www.smarty.net/
 */
class SmartyAdapter implements ViewsAdapterInterface
{
    /**
     * Smarty engine instance
     *
     * @var \Smarty
     */
    protected $smarty = null;

    /**
     * Templates' variables
     *
     * @var array
     */
    protected $data = [];

    /**
     * Set default directory for Smarty compiled files (i.e. the /templates_c)
     *
     * @var string
     */
    protected $defaultCompileDir = './../var/compile/smarty/';

    /**
     * Default directory permissions for compile directory
     *
     * @var int
     */
    protected $permissions = 0770;

    /**
     * Initialization
     *
     * @return SmartyAdapter
     */
    public function init()
    {
        if ($this->smarty) {
            return $this;
        }

        $app = Application::getInstance();
        $cfg = $app->getConfig();

        $viewsPath = $cfg->get('app.views.path', '../app/Views/');
        $viewsPath = realpath($viewsPath) . '/';

        $compileDir = $this->defaultCompileDir;
        $compileDir = $cfg->get('app.views.options.smarty.compile_dir', $compileDir);
        if (!file_exists($compileDir)) {
            @mkdir($compileDir, $this->permissions, true);
        }
        $compileDir = realpath($compileDir) . '/';

        $errorReport = !$cfg->get('app.debug', false)
            ? 0
            : E_ALL & ~E_NOTICE;

        $this->smarty = new Smarty();
        $this->smarty
            ->setTemplateDir($viewsPath)
            ->setCompileDir($compileDir)
            ->setErrorReporting($errorReport);

        return $this;
    }

    /**
     * Set variables to the view
     *
     * @param mixed $key String or key-value Array
     * @param mixed $value If $key is a string, this is the value
     * @return SmartyAdapter
     */
    public function set($key = null, $value = null)
    {
        if (is_string($key)) {
            $this->data[$key] = $value;
        } elseif (is_array($key)) {
            $this->data = array_merge($this->data, $key);
        }

        return $this;
    }

    /**
     * Render the selected template
     *
     * @param string $filename Template filename, relative to the view's path
     * @return string
     */
    public function render($filename)
    {
        $this->smarty->assign($this->data);
        return $this->smarty->fetch($filename);
    }
}
