<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use Exception;
use Kofenium\Singleton;
use Kofenium\FilterData;
use InvalidArgumentException;

/**
 * Registry pattern class for configurations
 *
 * Can be used as follows:
 * <pre>
 * $cfg = \Kofenium\Config::getInstance();
 * $path = $cfg->get('app.views.path', false);
 * $path = $cfg->app['views']['path'];         // through __get
 * $cfg->set('database.driver', 'mysql');
 * $cfg->setArray([
 *     'database.connections.mysql.username' => 'username',
 *     'database.connections.mysql.password' => 'password'
 * ]);
 * echo $cfg->get('app.unlisted_config', 'default_value');
 * print_r($cfg->find('database.connections.mysql', true));
 * </pre>
 */
final class Config extends Singleton
{
    /**
     * Valid directory path to configration files
     *
     * @var string
     */
    protected $path = null;

    /**
     * Default directory path for configration files
     *
     * @var string
     */
    protected $defaultPath = null;

    /**
     * List of all loaded configuration settings. Multidimensional array
     *
     * @var array
     */
    protected $settings = array();

    /**
     * Add default path to search for
     *
     * @param string $path Path to be added
     * @return Config
     * @throws \InvalidArgumentException On invalid variable type of $path
     */
    public function addPath($path)
    {
        if (!$path || !is_string($path)) {
            throw new InvalidArgumentException('Expected string argument $path. Passed: ' . $path);
        }
        $this->defaultPath = $path;
        return $this;
    }

    /**
     * Set default path as current used
     *
     * @throws \Exception When path isn't reachable (not found, isn't directory or can't be read)
     */
    public function setPath()
    {
        $dir = realpath($this->defaultPath);
        if (!$dir || !is_dir($dir) || !is_readable($dir) ) {
            throw new Exception('Directory was not found or inaccessible: ' . $this->defaultPath);
        }

        $this->settings = [];
        $this->path = $dir . DIRECTORY_SEPARATOR;
        return $this;
    }

    /**
     * Get direct config setting, if exists/loaded
     *
     * @param string
     */
    public function __get($name)
    {
        if ($this->isPathUnset()) {
            $this->setPath();
        }

        if (!isset($this->settings[$name])) {
            $this->loadFile($this->path . $name . '.php');
        }

        if (isset($this->settings[$name])) {
            return $this->settings[$name];
        }

        return null;
    }

    /**
     * Get setting through dot-based hierarchy name
     *
     * @param string $name Setting name (dotted syntax)
     * @param mixed $default Default value if setting was not found
     * @param string $filter Filter data by using pipe-separated filters
     * @return mixed
     */
    public function get($name, $default = null, $filter = null)
    {
        $config = rtrim($name, '.');
        $file = $this->loadCategory($config);

        // convert multi dim array to single dimension with dot-based keys
        $list = $this->getDotsFromArray([$file => $this->settings[$file]]);

        $data = isset($list[$config])
            ? $list[$config]
            : $default;

        return $filter !== null
            ? FilterData::normalize($data, $filter)
            : $data;
    }

    /**
     * Get list of config settings through dot-based hierarchy name
     *
     * Depends on $stripRootPath flag, the find() method will return different
     * keys in the array. For example if the test string is
     * **'category.sub.sub.array_list'** and the $stripRootPath = **False** (i.e. default):
     *
     * <pre>
     * [
     *     'category.sub.sub.array_list.port' => 1234,
     *     'category.sub.sub.array_list.options' => [
     *         [0] => 'string',
     *         [1] => 4.44,
     *         [100] => 'specific key',
     *     ]
     *     'category.sub.sub.array_list.timeout' => 3600,
     * ]
     * </pre>
     * whereas $stripRootPath = **True** will return:
     * <pre>
     * [
     *     'port' => 1234,
     *     'options' => [
     *         [0] => 'string',
     *         [1] => 4.44,
     *         [100] => 'specific key',
     *     ]
     *     'timeout' => 3600,
     * ]
     * </pre>
     *
     * @param string $name Config name to search for (dot-notation path)
     * @param bool $stripRootPath Remove the root path from the array keys (i.e. $name)
     * @return mixed
     */
    public function find($name, $stripRootPath = false)
    {
        $file = $this->loadCategory($name);

        // convert multi dim array to single dimension with dot-based keys
        $list = $this->getDotsFromArray([$file => $this->settings[$file]]);

        ksort($list);

        // get whole config file
        if ($name === $file) {
            return $stripRootPath ? $this->stripRootPath($list, $name) : $list;
        }

        $result = [];

        foreach ($list as $key => $item) {
            if ($key === $name) {
                $result = array_replace_recursive($result, (array) $item);
            }
            elseif (strpos($key, rtrim($name, '.') . '.') === 0) {
                $result[$key] = $item;
            }
        }
        return $stripRootPath ? $this->stripRootPath($result, $name) : $result;
    }

    /**
     * Get direct config setting, if exists/loaded
     *
     * @param string $name Setting name or multidimensional array
     * @param mixed $value Value
     * @return Config
     */
    public function __set($name, $value)
    {
        $this->loadCategory($name);
        $this->settings[$name] = array_replace_recursive(
            $this->settings[$name],
            is_array($value) ? $value : [$value]
        );
        return $this;
    }

    /**
     * Set value in dot-based hierarchy name
     *
     * @param string $name Config setting name
     * @param mixed $value A value to be added
     * @return Config
     */
    public function set($name, $value = null)
    {
        $this->loadCategory($name);
        $extend = $this->getArrayFromDots($name, $value);
        $this->settings = array_replace_recursive($this->settings, $extend);
        return $this;
    }

    /**
     * Set array values in dot-based hierarchy name
     *
     * @param array $values
     * @return Config
     */
    public function setArray(array $values)
    {
        if (!count($values)) {
            return $this;
        }
        ksort($values);

        $lastCategory = '';
        $extend = [];

        foreach ($values as $key => $value) {
            $file = explode('.', $key);
            if ($lastCategory !== $file[0]) {
                $this->loadCategory($key);
                $lastCategory = $file[0];
            }

            $extend = array_replace_recursive(
                $extend,
                $this->getArrayFromDots($key, $value)
            );
        }

        $this->settings = array_replace_recursive($this->settings, $extend);
        return $this;
    }

    /**
     * Return config path set status
     *
     * @return boolean True if path isn't set yet
     */
    public function isPathUnset()
    {
        return $this->path === null;
    }

    /**
     * Load config file
     *
     * @param string $filename Filename (w/ full path) to load settings from
     * @throws InvalidArgumentException If $filename isn't string
     * @throws Exception If provided file isn't reachable (not found, isn't file or can't be read)
     */
    protected function loadFile($filename)
    {
        if (!$filename || !is_string($filename)) {
            throw new InvalidArgumentException('Expected string argument in $filename. Passed: ' . $filename);
        }

        $file = realpath($filename);
        if (!$file || !is_file($file) || !is_readable($file)) {
            throw new Exception('File was not found or inaccessible: ' . $filename);
        }

        $name = explode('.php', basename($file))[0];
        $this->settings[$name] = include $file;
    }

    /**
     * Get dot-notation array from multidimensional
     *
     * @param array $current Multi Dimensional array
     * @param array $next
     * @param string $nkey
     * @return string
     */
    private function getDotsFromArray($current, $next = array(), $nkey = '')
    {
        static $method = __FUNCTION__;

        $keyIndex = 0;
        foreach ($current as $key => $value) {
            if (is_array($value)) {
                $next = array_merge($next, $this->$method($value, $next, $nkey . $key . '.'));
                continue;
            }

            if (is_int($key)) {
                $name = rtrim($nkey, '.');

                if (!isset($next[$name])) {
                    // if the index is first and single, assign value direct
                    if ($key > 0) {
                        $next[$name][$key] = $value;
                    } else {
                        $next[$name] = $value;
                        $keyIndex = $key;
                    }
                } else if (!is_array($next[$name])) {
                    // hold future elements into array
                    $next[rtrim($nkey, '.')] = [
                        $keyIndex => $next[rtrim($nkey, '.')],
                        $key => $value
                    ];
                } else {
                    // next element to the array
                    $next[rtrim($nkey, '.')][$key] = $value;
                }
                continue;
            }

            $next[$nkey . $key] = $value;
        }

        return $next;
    }

    /**
     * Get Multidimension array from dot-notation string
     *
     * @param string $name dot-notation string
     * @param mixed $value
     * @return array
     */
    private function getArrayFromDots($name, $value = null)
    {
        $keys = explode('.', $name);

        $source = [];
        $sourceRef = &$source;

        foreach ($keys as $key) {
            $sourceRef = &$sourceRef[$key];
        }

        $sourceRef = $value;
        return $source;
    }

    /**
     * Load file from dotted config name, as "category" is the first part of the dotted string
     *
     * @param string $name
     * @return string
     * @throws \InvalidArgumentException If $name isn't a string
     */
    private function loadCategory($name)
    {
        if (!$name || !is_string($name)) {
            throw new InvalidArgumentException('Expected string argument in $name. Passed: ' . $name);
        }

        $parts = explode('.', $name);
        $file = $parts[0];

        if ($this->isPathUnset()) {
            $this->setPath();
        }

        if (!isset($this->settings[$file])) {
            $this->loadFile($this->path . $file . '.php');
        }

        return $file;
    }

    /**
     * Strip text from each dot-notation array's key
     *
     * @param array $array Key-Value dot-notation array
     * @param string $rootPath The root path text which to be removed from the keys
     * @return array
     * @see Config::find() Method find()
     */
    private function stripRootPath($array, $rootPath = '')
    {
        $nameLength = strlen(rtrim($rootPath, '.'));
        $result = [];
        foreach ($array as $key => $value) {
            $key = substr($key, $nameLength + 1) ?: $key;
            $result[$key] = $value;
        }
        return $result;
    }
}
