<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use Kofenium\Views\View;
use Kofenium\Application;
use Kofenium\Router\RouterInterface;

/**
 * Base controller
 */
abstract class Controller
{
    /**
     * Application Instance
     * @var \Kofenium\Application
     */
    protected $app;

    /**
     * Configs instance
     * @var \Kofenium\Config
     */
    protected $config;

    /**
     * Views instance
     * @var \Kofenium\Views\View
     */
    protected $view;

    /**
     * Router instance
     * @var \RouterInterface
     */
    protected $router;

    /**
     * Init properties
     */
    public function __construct()
    {
        $this->app = Application::getInstance();
        $this->config = $this->app->getConfig();
        $this->view = View::getInstance();
        $this->router = $this->app->getRouter();
    }
}
