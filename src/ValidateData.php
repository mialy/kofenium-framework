<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use Closure;
use Exception;
use InvalidArgumentException;

/**
 * Basic data validation class
 *
 * Can be used as follows:
 * <pre>
 * // $_POST variables from submitted form
 * $emailAddress = \Kofenium\Input::post('email_address');
 * $age = \Kofenium\Input::post('age', 0);
 *
 * // ValidateDate instance
 * $rules = new ValidateData();
 *
 * // Set some rules - method chaining
 * $rules->setRule('email_addr', 'email', $emailAddress, null, 'Invalid Email address!')
 *     ->setRule('age', 'between', null, [18, 120], "{0} years old? Your age must be between {1} and {2}.");
 * // group's value can be set later
 * $rules->setValue('age', $age);
 *
 * // run all rules
 * if (!$rules->run()) {
 *     // show errors
 *     foreach ($rules->getErrors() as $fieldName => $data) {
 *          echo "[$fieldName] - (".$data['method'].'): '. $data['message'] . "\n";
 *     }
 * }
 *
 * // To call method directly:
 * if (!\Kofenium\ValidateData::minLength($input, 3)) {
 *     echo "Enter at least 3 characters!";
 * }
 * </pre>
 *
 * To invert method' status, simply add ! before the name, i.e. 'inArray' => '!inArray'.
 *
 * List of the supported methods (including their short/long aliases):
 * <ul>
 *  <li>alpha</li>
 *  <li>alphaNumeric, alphanum</li>
 *  <li>betweenLengthValues, between_length</li>
 *  <li>betweenValues, between</li>
 *  <li>custom, closure, function</li>
 *  <li>equal</li>
 *  <li>equalExact</li>
 *  <li>email, is_email</li>
 *  <li>exactLength, exact</li>
 *  <li>gt, &gt;</li>
 *  <li>gte, &gt;=</li>
 *  <li>inArray, in, contain, contains</li>
 *  <li>ip</li>
 *  <li>ipv4</li>
 *  <li>ipv6</li>
 *  <li>isBoolean, is_boolean, bool, is_bool</li>
 *  <li>isFloat, float, is_float, double, is_double</li>
 *  <li>isInteger, is_integer, int, is_int</li>
 *  <li>isRegex, is_regex</li>
 *  <li>lt, &lt;</li>
 *  <li>lte, &gt;=</li>
 *  <li>mac</li>
 *  <li>maxLength, max</li>
 *  <li>minLength, min</li>
 *  <li>regexp, regex</li>
 *  <li>required, require</li>
 *  <li>url, is_url</li>
 * </ul>
 *
 * See the all static methods for details.
 */
class ValidateData
{
    /**
     * List of active rules
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Errors list after calling ValidateData::run() or ValidateData::validate()
     *
     * @var array
     * @see ValidateData::getErrors() Format of errors
     */
    protected $errors = [];

    /**
     * List of error strings for each method or group->method
     *
     * @var array
     */
    protected $messages = [];

    /**
     * Set method aliases
     *
     * @var array
     */
    protected $aliases = [
        'min' => 'minLength',
        'max' => 'maxLength',
        'between_length' => 'betweenLengthValues',
        'between' => 'betweenValues',
        'exact' => 'exactLength',
        '<=' => 'lte',
        '>=' => 'gte',
        '<' => 'lt',
        '>' => 'gt',
        'require' => 'required',

        'in' => 'inArray',
        'contain' => 'inArray',
        'contains' => 'inArray',

        'is_email' => 'email',
        'is_url' => 'url',

        'is_regex' => 'isRegex',
        'regex' => 'regexp',

        'closure' => 'custom',
        'function' => 'custom',

        // booleans
        'bool' => 'isBoolean',
        'boolean' => 'isBoolean',
        'is_bool' => 'isBoolean',

        // integers
        'int' => 'isInteger',
        'integer' => 'isInteger',
        'is_int' => 'isInteger',
        'is_integer' => 'isInteger',

        // floats
        'float' => 'isFloat',
        'is_float' => 'isFloat',

        // double
        'double' => 'isFloat',
        'is_double' => 'isFloat',
    ];


    /**
     * Construct
     *
     * @param array $rules List of rules
     * @param array $values List of values for each rule's group
     * @param array $messages List of error messages for each rule's group
     */
    public function __construct(array $rules = [], array $values = [], array $messages = [])
    {
        $this->setRules($rules);
        $this->setValues($values);
        $this->setMessages($messages);
    }

    /**
     * Get error for spcified group after ::run() or ::validate() call
     *
     * @param string $name Group name
     * @return array As follows:
     * <pre>
     * [
     *     'method' => 'regex',
     *     'message' => 'Invalid date. Please use the following format: YYYY-MM-DD'
     * ]
     * </pre>
     */
    public function getError($name)
    {
        if (!isset($this->errors[$name])) {
            return [];
        }

        return $this->errors[$name];
    }

    /**
     * Get all errors after run() or validate() call
     *
     * @see ValidateData::run() Method run()
     * @see ValidateData::validate() Method validate()
     * @return array As follows:
     * <pre>
     * [
     *     [email] => [
     *         'method' => 'email',
     *         'message' => 'Please enter a valid E-mail address.'
     *     ],
     *     [username] => [
     *         'method' => 'min',
     *         'message' => 'Please enter at least 2 characters.'
     *     ],
     * ]
     * </pre>
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get error messages for specified method
     *
     * @param string $method Method name
     * @return string
     */
    public function getMessage($method)
    {
        return isset($this->messages[$method]) ? $this->messages[$method] : null;
    }

    /**
     * Set error message for specified method
     *
     * Method name is one of the string formats: 'method' or 'group_name.method'.
     * In first case, 'method' is global for all methods within rule table, whereas
     * the second format restrict message to specific group name. Priority of the
     * errors are as follow:
     * <ol>
     *  <li>The message within group, within rule in Rules array [higher]</li>
     *  <li>The message as 'group.method' in Messages array</li>
     *  <li>The message as 'method'-only in Messages array [lower]</li>
     * </ol>
     *
     * The message may contains placeholders such as {0}, {1}, {2}, etc. They
     * represents current value (0), and the checker values as 1 (first), 2 (second), etc.
     *
     * @param string $method Method name
     * @param string $message Error Message
     * @return ValidateData
     */
    public function setMessage($method, $message)
    {
        $this->messages[trim($method)] = $message;
        return $this;
    }

    /**
     * Get all error messages
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set error messages
     *
     * The message array looks like:
     * <pre>
     * [
     *     'method' => 'error message',
     *     'nextMethod' => 'another error message',
     *     'anotherMethod' => 'Expected value between {1} and {2}, provided {0}!',
     *     // etc.
     * ]
     * </pre>
     *
     * @param array $messages List of errors messages as method => message values
     * @return  ValidateData
     * @see ValidateData::setMessage() Message format
     */
    public function setMessages(array $messages)
    {
        if (!count($messages)) {
            return $this;
        }

        foreach ($messages as $method => $message) {
            $this->messages[trim($method)] = $message;
        }

        return $this;
    }

    /**
     * Get rule
     *
     * @param string $name Group name for the rule
     * @param mixed $method Specific method (either as name, or as index starts from #0)
     * @return array
     */
    public function getRule($name, $method = null)
    {
        if (!isset($this->rules[$name])) {
            return [];
        }

        if (is_null($method)) {
            return $this->rules[$name]['rules'];
        } elseif (is_integer($method)) {
            return isset($this->rules[$name]['rules'][$method])
                ? [$this->rules[$name]['rules'][$method]] // as 0 index
                : [];
        } elseif (is_string($method)) {
            $method = trim($method);
            $result = [];
            foreach ($this->rules[$name]['rules'] as $rule) {
                if ($rule['method'] === $method) {
                    $result[] = $rule;
                }
            }
            return $result;
        } else {
            return [];
        }
    }

    /**
     * Set rule
     *
     * @param string $name Group name for the rule; one name may contains multiple rules
     * @param string $method Which validation should be used, may contain 'custom' for closures
     * @param mixed $value Input value to check for
     * @param mixed $checker Value used by the method to detemine if input value is correct.
     *      If $method is 'custom', this must contain a closure.
     * @param string $message Error message for this rule
     * @return ValidateData
     */
    public function setRule($name, $method, $value = null, $checker = null, $message = null)
    {
        $this->rules[$name]['value'] = $value;
        $this->rules[$name]['rules'][] = [
            'method' => trim($method),
            'value' => $checker,
            'message' => $message,
        ];

        return $this;
    }

    /**
     * Get list of currently set rules
     *
     * @param string $name Group name of the rules
     * @return array
     */
    public function getRules($name = null)
    {
        if ($name) {
            return isset($this->rules[$name])
                ? $this->rules[$name]
                : null;
        }
        return $this->rules;
    }

    /**
     * Set rules list
     *
     * The rules array looks like:
     * <pre>
     * [
     *     'family_name' => [    // group name (i.e. form's field name)
     *         // validation methods list...
     *         'min' => 3,       // check value
     *         'max' => [60],    // single element is always the check value
     *         '!inArray' => [ // indexed array
     *             // #0 - value to check for
     *             ['alfred', 'payne', 'aflek'],
     *
     *             // #1 - the error message
     *             'See blacklisted names.'
     *         ],
     *         'regex' => [      // explicitly array
     *             'value' => '/[a-z\'\-]/i',
     *             'message' => 'Use latin letters-only.'
     *         ],
     *         'custom' => [     // closure
     *             'message' => 'message_on_error'
     *
     *              // closures must have single argument
     *             'value' => function($value) use ($myVar) {
     *                 return $value === $myVar;
     *             }
     *         ]
     *     ],
     *     // next group...
     * ];
     * </pre>
     *
     * @param array $rulesList List of rules to add
     * @return ValidateData
     */
    public function setRules(array $rulesList)
    {
        if (!count($rulesList)) {
            return $this;
        }

        foreach ($rulesList as $name => $rules) {
            foreach ($rules as $method => $rule) {
                $checker = null;
                $message = null;

                if (is_array($rule) && count($rule) === 2) {
                    if (isset($rule[0])) {
                        $checker = $rule[0];
                    } elseif (isset($rule['value'])) {
                        $checker = $rule['value'];
                    }

                    if (isset($rule[1])) {
                        $message = $rule[1];
                    } elseif (isset($rule['message'])) {
                        $message = $rule['message'];
                    }
                } elseif (is_array($rule) && count($rule) === 1) {
                    if (isset($rule[0])) {
                        $checker = $rule[0];
                    } elseif (isset($rule['value'])) {
                        $checker = $rule['value'];
                    } elseif (isset($rule['message'])) {
                        $message = $rule['message'];
                    }
                } else {
                    $checker = $rule;
                }

                $this->setRule($name, $method, null, $checker, $message);
            }
        }

        return $this;
    }

    /**
     * Get value of the group rules
     *
     * @param string $name Group name
     * @return mixed
     */
    public function getValue($name)
    {
        return isset($this->rules[$name]['value']) ? $this->rules[$name]['value'] : null;
    }

    /**
     * Set value for group rules
     *
     * @param string $name Group name
     * @param mixed $value Input value
     * @return ValidateData
     */
    public function setValue($name, $value)
    {
        $this->rules[$name]['value'] = $value;
        return $this;
    }

    /**
     * Get all values
     *
     * @return array Array of ['group_name' => 'value']
     */
    public function getValues()
    {
        if (!count($this->rules)) {
            return [];
        }

        $values = [];
        foreach ($this->rules as $name => $rule) {
            $values[$name] = isset($rule['value']) ? $rule['value'] : null;
        }

        return $values;
    }

    /**
     * Set values for group rules
     *
     * The message array looks like:
     * <pre>
     * [
     *     'group_name' => 'value',
     *     'group_name_2' => 12345,
     *     // etc.
     * ]
     * </pre>
     *
     * @param array $values Array list of values as ['group_name' => 'value']
     * @return ValidateData
     */
    public function setValues(array $values)
    {
        if (!count($values)) {
            return $this;
        }

        foreach ($values as $name => $value) {
            $this->rules[$name]['value'] = $value;
        }

        return $this;
    }

    /**
     * Reset the instance to it's original configuration
     *
     * @return ValidateData
     */
    public function reset()
    {
        $this->rules = [];
        $this->errors = [];

        return $this;
    }

    /**
     * Validate data, based on set rules
     *
     * @return bool True if everything is successfully passed, or false - on error
     * @throws \Exception If validation method not found
     */
    public function run()
    {
        $this->errors = [];

        if (!count($this->rules)) {
            // Calling run() without any rules? It's something wrong?
            return false;
        }

        foreach ($this->rules as $name => $data) {
            $value = $data['value'];

            if (!isset($data['rules'])) {
                continue;
            }

            foreach ($data['rules'] as $rules) {
                $method = $rules['method'];
                $isInverted = isset($method[0]) && $method[0] === '!';

                if ($isInverted) {
                    $method = substr($method, 1);
                }

                if (isset($this->aliases[strtolower($method)])) {
                    $method = $this->aliases[strtolower($method)];
                }

                $callable = [get_class(), $method];
                if (!is_callable($callable)) {
                    throw new Exception("Validation method not found: " . $method);
                }

                $arguments = [$value, $rules['value']];
                $status = call_user_func_array($callable, $arguments);

                if ($isInverted xor (!$status)) {
                    $this->errors[$name] = [
                        'method' => $method,
                        'message' => $this->forgeMessage($name, $method, $arguments, $rules['message']),
                    ];

                    // pass the rest errors for this group
                    break;
                }
            }
        }

        return (bool) !count($this->errors);
    }

    /**
     * Alias of run()
     *
     * @see ValidateData::run() Method run()
     * @return bool
     */
    public function validate()
    {
        return $this->run();
    }

    /**
     * Forge error message after error found
     *
     * @param string $name Group name
     * @param string $method Method name
     * @param array $values List of rule values. First parameter is the input value
     * @param string $message  Default group->rule message
     * @return string
     */
    protected function forgeMessage($name, $method, array $values = [], $message = null)
    {
        $error = '';
        $nameMethod = $name . '.' . $method;

        if (mb_strlen($message)) {
            $error = $message;
        } elseif (isset($this->messages[$nameMethod]) && mb_strlen($this->messages[$nameMethod])) {
            $error = $this->messages[$nameMethod];
        } elseif (isset($this->messages[$method]) && mb_strlen($this->messages[$method])) {
            $error = $this->messages[$method];
        }

        if (!count($values)) {
            return $error;
        }

        $values = array_values($values);

        $i = -1;
        foreach ($values as $value) {
            $i++;

            if (is_string($value) || is_numeric($value)) {
                $error = preg_replace('/\{' . $i . '}/u', $value, $error);
                continue;
            } elseif (!is_array($value)) {
                continue;
            }

            foreach ($value as $v) {
                if (!is_string($v) && !is_numeric($v)) {
                    $i++;
                    continue;
                }
                $error = preg_replace('/\{' . $i . '}/u', $v, $error);
                $i++;
            }
        }

        return $error;
    }

    /**
     * Check if input is alpha (a-z, case-insensitive)
     *
     * @param string $value Input
     */
    public static function alpha($value)
    {
        return (bool) preg_match('/^[a-z]+$/i', $value);
    }

    /**
     * Check if input is alphanumeric (a-z and 0-9, case-insensitive)
     *
     * @param string $value Input
     * @return bool True if input is alpha-numeric value only
     */
    public static function alphaNumeric($value)
    {
        return (bool) preg_match('/^[a-z\d]+$/i', $value);
    }

    /**
     * Check if value is between two numbers
     *
     * @param string $value The value for being checked
     * @param array $numbers 'From' and 'To' values to check for
     * @return bool True if value is between range, false - otherwise
     */
    public static function betweenValues($value, array $numbers)
    {
        $from = min($numbers[0], $numbers[1]);
        $to = max($numbers[0], $numbers[1]);
        return $value >= $from && $value <= $to;
    }

    /**
     * Check value with custom function (closure)
     *
     * @param mixed $value The value for being checked
     * @param closure $callback Closure for checking. Closures must contain single argument.
     * @return bool True for success, false - otherwise (closure depends)
     * @throws InvalidArgumentException If $callback isn't closure
     */
    public static function custom($value, $callback)
    {
        if ($callback instanceof Closure) {
            return (bool) call_user_func($callback, $value);
        }

        throw new InvalidArgumentException("Expected closure!");
    }

    /**
     * Check if value is equal (regardless of variable's type)
     *
     * @param mixed $value Input
     * @param mixed $compare Compare value
     * @return bool True if input string is equal to compare value
     */
    public static function equal($value, $compare)
    {
        return $value == $compare;
    }

    /**
     * Check if value is equal (with variable's type respect)
     *
     * @param mixed $value Input
     * @param mixed $length Length in characters
     * @return bool True if input string is equal to compare value and the variable types match
     */
    public static function equalExact($value, $compare)
    {
        return $value === $compare;
    }

    /**
     * Check if value is valid Email
     *
     * @param string $value Email address
     * @return bool True on valid Email address, false - otherwise
     */
    public static function email($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * Check value's exact length
     *
     * @param string $value Input string
     * @param integer $length Length in characters
     * @return bool True if input string is exact $length chars, false - otherwise
     */
    public static function exactLength($value, $length)
    {
        return mb_strlen($value, 'UTF-8') === ((int) $length);
    }

    /**
     * Check if input is greater-than compared value
     *
     * @param mixed $value Input
     * @param mixed $compare Value to compare to
     * @return bool True if $value is great than $compare
     */
    public static function gt($value, $compare)
    {
        return $value > $compare;
    }

    /**
     * Check if input is greater-than or equal to compared value
     *
     * @param mixed $value Input
     * @param mixed $compare Value to compare to
     * @return bool True if $value is great than or equal to $compare
     */
    public static function gte($value, $compare)
    {
        return $value >= $compare;
    }

    /**
     * Check if value exists in array
     *
     * @param mixed $value Input
     * @param array $list Whitelist values
     * @return bool True if value exists in array, false - otherwise
     */
    public static function inArray($value, array $list)
    {
        return in_array($value, $list);
    }

    /**
     * Check if value is valid IP address
     *
     * @param string $value IP Address
     * @param int $version Specific IP version: 4 or 6
     * @return bool True if value is valid IP, false - otherwise
     */
    public static function ip($value, $version = null)
    {
        if ($version === 4) {
            return self::ipv4($value);
        } elseif ($version === 6) {
            return self::ipv6($value);
        } elseif ($version !== null) {
            throw new Exception("Supported IP versions: 4 and 6. Provided: " . (int) $version);
        }

        return filter_var($value, FILTER_VALIDATE_IP) !== false;
    }

    /**
     * Check if value is valid IPv4 address
     *
     * @param string $value IPv4 Address
     * @return bool True if value is valid IPv4, false - otherwise
     */
    public static function ipv4($value)
    {
        return filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false;
    }

    /**
     * Check if value is valid IPv6 address
     *
     * @param string $value IPv6 Address
     * @return bool True if value is valid IPv6, false - otherwise
     */
    public static function ipv6($value)
    {
        return filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false;
    }

    /**
     * Check if value is boolean
     *
     * @param mixed $value Input value
     * @return bool True if value is boolean, false - otherwise
     */
    public static function isBoolean($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN) !== false;
    }

    /**
     * Check if value is float or double
     *
     * @param mixed $value Input value
     * @return bool True if value is float or double, false - otherwise
     */
    public static function isFloat($value)
    {
        return filter_var($value, FILTER_VALIDATE_FLOAT) !== false;
    }

    /**
     * Check if value is integer
     *
     * @param mixed $value Input value
     * @return bool True value is integer, false - otherwise
     */
    public static function isInteger($value)
    {
        return filter_var($value, FILTER_VALIDATE_INT) !== false;
    }

    /**
     * Check if value is valid preg-compatibility regular expression string
     *
     * @param string $value Regular expression (regex) string
     * @return bool True if value is valid RegExp, false - otherwise
     */
    public static function isRegex($value)
    {
        return filter_var($value, FILTER_VALIDATE_REGEXP) !== false;
    }

    /**
     * Check if input is less-than compared value
     *
     * @param mixed $value Input
     * @param mixed $compare Value to compare to
     * @return bool True if $value is less than $compare
     */
    public static function lt($value, $compare)
    {
        return $value < $compare;
    }

    /**
     * Check if input is less-than or equal to compared value
     *
     * @param mixed $value Input
     * @param mixed $compare Value to compare to
     * @return bool True if $value is less than or equal to $compare
     */
    public static function lte($value, $compare)
    {
        return $value <= $compare;
    }

    /**
     * Check if value is valid MAC address
     *
     * @param string $value MAC Address
     * @return bool True on valid MAC, false - otherwise
     */
    public static function mac($value)
    {
        return filter_var($value, FILTER_VALIDATE_MAC) !== false;
    }

    /**
     * Check if value's length is between two numbers
     *
     * @param string $value The value for being checked
     * @param array $numbers 'From' and 'To' values to check for
     * @return bool True if value is between range, false - otherwise
     */
    public static function betweenLengthValues($value, array $numbers)
    {
        $length = mb_strlen((string) $value);
        $from = min($numbers[0], $numbers[1]);
        $to = max($numbers[0], $numbers[1]);
        return $length >= $from && $length <= $to;
    }

    /**
     * Check if value has maximum characters
     *
     * @param string $value The value for being checked
     * @param integer $length Maximum length of characters
     * @return bool True on success, false - otherwise
     */
    public static function maxLength($value, $length)
    {
        return mb_strlen((string) $value) <= (int) $length;
    }

    /**
     * Check if value has minimum characters
     *
     * @param string $value The value for being checked
     * @param integer $length Minimum length of characters
     * @return bool True on success, false - otherwise
     */
    public static function minLength($value, $length)
    {
        return mb_strlen((string) $value) >= (int) $length;
    }

    /**
     * Check value with regular expression
     *
     * @param string $value Input
     * @param string $regexp Regular expression (regex) string
     * @return bool True on success match, false - otherwise
     */
    public static function regexp($input, $regexp)
    {
        return (bool) preg_match($regexp, $input);
    }

    /**
     * Check if input is NOT empty
     *
     * @param array|string|int $input Input value
     * @return bool True if the input isn't empty
     */
    public static function required($input)
    {
        return is_array($input) ? !empty($input) : ($input != '');
    }

    /**
     * Check if value is valid URL
     *
     * @param string $value URL address
     * @return bool True on valid URL, false - otherwise
     */
    public static function url($value)
    {
        return filter_var($value, FILTER_VALIDATE_URL) !== false;
    }
}
