<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use Closure;
use Kofenium\Views\View;
use InvalidArgumentException;
use Kofenium\Router\RouterInterface;

/**
 * Accept all inputs and call the found controller
 */
class FrontController extends Singleton
{
    /**
     * Instance of router
     *
     * @var \RouterInterface
     */
    protected $router;

    /**
     * Request
     *
     * @var string
     */
    protected $request;

    /**
     * Request Method
     *
     * @var string
     */
    protected $requestMethod = null;

    /**
     * Setup router
     *
     * @param \RouterInterface Router instance
     */
    public function setRouter(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * Set request string
     *
     * @param string $request Request string
     */
    public function setRequest($request = null)
    {
        $this->request = $request;
    }

    /**
     * Set request string
     *
     * @param string $request Request string
     */
    public function setRequestMethod($requestMethod = null)
    {
        $this->requestMethod = $requestMethod;
    }

    /**
     * Dispatch controllers, based on routes
     *
     * @param string $defaultRoute Default route's target if none of the existed routes match the request
     * @throws \InvalidArgumentException If provided target is invalid
     */
    public function dispatch($defaultRoute = '')
    {
        $output = null;
        $namespace = null;

        if (($result = $this->router->match($this->request, $this->requestMethod)) === false) {
            if (empty($defaultRoute)) {
                return;
            }

            $result = [
                'target' => $defaultRoute,
                'params' => [404],
            ];
        }

        $middleware = null;

        if ($result['target'] instanceof Closure || is_callable($result['target'])) {
            $callback = $result['target'];
        } elseif (is_string($result['target']) && strpos($result['target'], '@') > 0) {
            $callback = $this->getCallback($result['target'], $namespace, $initClass);
        } elseif (is_array($result['target']) && isset($result['target']['target'])) {
            if (isset($result['target']['namespace'])) {
                $namespace = $result['target']['namespace'];
            }

            if (isset($result['target']['middleware'])) {
                $middleware = $result['target']['middleware'];
            }

            $callback = $this->getCallback($result['target']['target'], $namespace, $initClass);
        }

        if ($middleware) {
            foreach ((array) $middleware as $target) {
                $target = $this->getCallback($target, $namespace, $middleInitClass);

                if (!empty($target) && is_array($target) && $middleInitClass) {
                    $target = [
                        new $target[0],
                        $target[1]
                    ];
                }

                if (!$target || call_user_func($target) === false) {
                    $result['params'] = [404];
                    $callback = $this->getCallback($defaultRoute, $namespace, $initClass);
                    break;
                }
            }
        }

        if (!empty($callback) && is_array($callback) && $initClass) {
            $callback = [
                new $callback[0],
                $callback[1]
            ];
        }

        if (empty($callback) || !is_callable($callback)) {
            throw new InvalidArgumentException("The Route target is not valid!", 500);
        }

        // output
        $output = call_user_func_array($callback, $result['params']);

        if (is_string($output) || is_numeric($output)) {
            header('Content-type: text/html; charset=utf-8');
            echo $output;
        } elseif ($output instanceof View) {
            header('Content-type: text/html; charset=utf-8');
            echo (string) $output->output();
        } elseif (is_array($output)) {
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($output);
        }
    }

    /**
     * Get callback-ready (callee) variable by provided target variable (string or array)
     *
     * @param string|array $target The target
     * @param string|array $namespace Single or multiple namespaces, where the target could be if it's a class
     * @param bool $initClass Do the callback needs to be instanced first?
     * @return mixed
     */
    private function getCallback($target, $namespace = null, &$initClass = false)
    {
        $namespaces = $namespace ? $namespace : '\\App\\Controllers\\';

        if ($target instanceof Closure || is_callable($target)) {
            return $target;
        } elseif (!is_string($target)) {
            return;
        }

        foreach (['@', '::', '->', '.'] as $delimiter) {
            if (strpos($target, $delimiter) !== false) {
                list($controller, $method) = explode($delimiter, $target);

                if ($delimiter !== '::') {
                    $initClass = true;
                }

                break;
            }
        }

        if (!isset($controller)) {
            if (function_exists($target)) {
                return $target;
            }
            return;
        }

        // fallback namespace
        if (!class_exists($controller)) {
            foreach ((array) $namespaces as $namespace) {
                if (class_exists($namespace . $controller, true)) {
                    $controller = $namespace . $controller;
                    break;
                }
            }
        }

        if (!class_exists($controller, true)) {
            return;
        }

        return [
            $controller,
            $method
        ];
    }
}
