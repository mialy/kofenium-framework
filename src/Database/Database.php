<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Database;

use PDO;
use Kofenium\Application;

/**
 * PDO database wrapper
 */
class Database
{
    /**
     * Current connection name
     *
     * @var string
     */
    protected $connection = null;

    /**
     * PDO object
     *
     * @var PDO
     */
    private $db = null;

    /**
     * PDO statement
     *
     * @var \PDOStatement
     */
    private $statement = null;

    /**
     * Last prepared query
     *
     * @var string
     */
    private $query = '';

    /**
     * Last added parameters to query
     *
     * @var array
     */
    private $parameters = [];

    /**
     * Constructor
     *
     * @param mixed $connection PDO instance, or connection name from 'database.connections' list
     * @see \Kofenium\Config::__get() Configuration settings
     */
    public function __construct($connection = null)
    {
        $app = Application::getInstance();

        if ($connection instanceof PDO) {
            $this->db = $connection;
        } elseif (!empty($connection)) {
            $this->db = $app->getDatabaseConnection($connection);
            $this->connection = $connection;
        } else {
            $this->db = $app->getDatabaseConnection($this->connection);
        }
    }

    /**
     * Prepare SQL query
     *
     * @param string $sql SQL Query
     * @param array $parameters SQL Query parameters
     * @param array $pdoOptions List of specific PDO Option for the query
     * @return Database
     */
    public function prepare($sql, $parameters = [], $pdoOptions = [])
    {
        $this->statement = $this->db->prepare($sql, $pdoOptions);
        $this->parameters = $parameters;
        $this->query = $sql;
        return $this;
    }

    /**
     * Execute last prepared query
     *
     * @param array $parameters SQL Query parameters
     * @return Database
     */
    public function execute($parameters = [])
    {
        if ($parameters) {
            $this->parameters = $parameters;
        }

        $this->statement->execute($this->parameters);
        return $this;
    }

    /**
     * Fetch single record
     *
     * @param integer $pdoFetchType PDO Fetch mode
     * @return array
     */
    public function fetch($pdoFetchType = 0)
    {
        return $this->statement->fetch($pdoFetchType);
    }

    /**
     * Fetch single record as associative array
     *
     * @return array
     */
    public function fetchAssoc()
    {
        return $this->statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Fetch single record as enumerated array
     *
     * @return array
     */
    public function fetchRow()
    {
        return $this->statement->fetch(PDO::FETCH_NUM);
    }

    /**
     * Fetch column index from single record
     *
     * @param integer $column Column number
     * @return array
     */
    public function fetchColumn($column = 0)
    {
        return $this->statement->fetchColumn($column);
    }

    /**
     * Fetch single record as object
     *
     * @return array
     */
    public function fetchObject()
    {
        return $this->statement->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Fetch all records
     *
     * @param integer $pdoFetchType PDO Fetch mode
     * @return array
     */
    public function fetchAll($pdoFetchType = 0)
    {
        return $this->statement->fetchAll($pdoFetchType);
    }

    /**
     * Fetch all records as associative array
     *
     * @return array
     */
    public function fetchAllAssoc()
    {
        return $this->statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fetch all records as enumerated array
     *
     * @return array
     */
    public function fetchAllRow()
    {
        return $this->statement->fetchAll(PDO::FETCH_NUM);
    }

    /**
     * Fetch all records as object
     *
     * @return object
     */
    public function fetchAllObject()
    {
        return $this->statement->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Get ID or sequence value of the last inserted row
     *
     * @return integer
     */
    public function getLastInsertId()
    {
        return $this->db->lastInsertId();
    }

    /**
     * Get number of affected rows from INSERT, UPDATE or DELETE queries
     *
     * @return integer
     */
    public function getAffectedRows()
    {
        return $this->statement->rowCount();
    }

    /**
     * Get number of columns in the result set
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->statement->columnCount();
    }

    /**
     * Return last query build with prepared parameters
     *
     * @return string
     * @see Database::getQuery() Method getQuery()
     */
    public function __toString()
    {
        return $this->getQuery();
    }

    /**
     * Try to get last query build with prepared parameters
     *
     * @return string
     */
    public function getQuery()
    {
        $query = $this->query;
        $parameters = $this->parameters;

        if (!count($parameters) || !strlen($query)) {
            return $query;
        }

        static $search = ["\\", "'", '"', "\0", "\n", "\r", "\x1a"];
        static $replaceWith = ["\\\\", "\\'", "\\\"", "\\0", "\\n", "\\r", "\Z"];

        // longer keys first to avoid incorrect matching
        krsort($parameters);

        foreach ($parameters as $name => $value) {
            if (is_numeric($value)) {
                $value = (int) $value;
            } else {
                $value = "'" . str_replace($search, $replaceWith, $value) . "'";
            }

            $query = str_replace(":" . $name, $value, $query);
        }

        return $query;
    }
}
