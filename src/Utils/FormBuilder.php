<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Utils;

/**
 * Form Builder class
 *
 * Basic usage if used the 'Form' as alias:
 * <pre>
 * echo Form::open('/', ['enctype' => 'multipart/form-data']);
 *     // set default attributes
 *     Form::setAttributes(['class' => 'form-control'], 'input');
 *     Form::setAttributes(['class' => 'form-control'], 'select');
 *     Form::setAttributes(['class' => 'btn btn-default'], 'button');
 *
 *     echo Form::text("username", Form::escape($result['username']), ['placeholder' => 'Username']);
 *     echo Form::password("password", null, ['placeholder' => 'Password']);
 *
 *     echo Form::label("Select sex");
 *     echo Form::select("sex", ['male' => 'Male', 'female' => 'Female', 'other' => 'Other'], 'other');
 *
 *     // clear attributes
 *     echo Form::clearAttributes(['class'], 'input');
 *
 *     echo Form::label("Avatar");
 *     echo Form::file('avatar');
 *
 *     echo Form::label([Form::checkbox('disable_emails', $result['disable_emails']), "Disable the newsletter"]);
 *
 *
 *     echo Form::hidden('user', $result['id']);
 *     echo "\n";
 *     echo Form::submit("edit_profile", ["<i class=\"fa fa-send\"></i>", "Edit profile"], 'submit');
 * echo Form::close();
 * </pre>
 */
class FormBuilder
{
    /**
     * The output as string from the last build tag/element
     *
     * @var string
     */
    protected static $output = '';

    /**
     * List of default attributes
     *
     * @var array
     */
    protected static $attributes = [];

    /**
     * Set attribute for all or specific element
     *
     * @param string $name Attribute's name
     * @param string $value Attribute's value
     * @param string $element The element name, if not provided, the attribute will apply to all elements
     */
    public static function setAttribute($name, $value, $element = null)
    {
        if (!$element || !strlen($element) || !is_string($element)) {
            $element = '*';
        }
        self::$attributes[$element][$name] = $value;
    }

    /**
     * Get attribute for all or for specific element
     *
     * @param string $name Attribute's name
     * @param string $element The element name, if not provided, the attribute will apply to all elements
     * @return string
     */
    public static function getAttribute($name, $element = null)
    {
        if (!$element || !strlen($element) || !is_string($element)) {
            $element = '*';
        }

        if (isset(self::$attributes[$element][$name])) {
            $attr = self::$attributes[$element][$name];
        } elseif ($element !== '*' && isset(self::$attributes['*'][$name])) {
            $attr = self::$attributes['*'][$name];
        } else {
            $attr = '';
        }

        return $attr;
    }

    /**
     * Clear specific attribute only
     *
     * @param string $name Attribute name
     * @param string $element Element name. If provided - Attribute will be cleared from this element name
     */
    public static function clearAttribute($name, $element = null)
    {
        if ($element) {
            self::$attributes[$element][$name] = null;
        } else { // for all elements
            if (!count(self::$attributes)) {
                return new static();
            }

            foreach (self::$attributes as $element => $data) {
                self::$attributes[$element][$name] = null;
            }
        }
    }

    /**
     * Clear all attributes
     *
     * @param string $element Element name. If provided - Attributes will be cleared from this element name
     */
    public static function clearAllAttributes($element = null)
    {
        if ($element) {
            self::$attributes[$element] = [];
        } else {
            self::$attributes = [];
        }
    }

    /**
     * Set attributes for all or for specific element
     *
     * @param array $attributes Array of name=value list of attributes
     * @param string $element The element name, if not provided, the attribute will apply to all elements
     */
    public static function setAttributes(array $attributes, $element = null)
    {
        if (!$element || !strlen($element) || !is_string($element)) {
            $element = '*';
        }

        self::$attributes[$element] = array_merge(
            isset(self::$attributes[$element]) ? self::$attributes[$element] : [],
            $attributes
        );
    }

    /**
     * Get attributes for all or for specific element
     *
     * @param string $element The element name, if not provided, the attribute will apply to all elements
     * @return array
     */
    public static function getAttributes($element = null)
    {
        if (!$element || !strlen($element) || !is_string($element)) {
            $element = '*';
        }

        $attr = [];
        if ($element !== '*') {
            $attr = isset(self::$attributes['*'])
                ? self::$attributes['*']
                : $attr;
        }

        $attr = isset(self::$attributes[$element])
            ? array_merge($attr, self::$attributes[$element])
            : $attr;

        return $attr;
    }

    /**
     * Clear specific attributes
     *
     * @param array $attributes Specific attribute names
     * @param string $element Element name. If provided - Attribute will be cleared from this element name
     */
    public static function clearAttributes(array $attributes, $element = null)
    {
        if (!$element || !strlen($element) || !is_string($element)) {
            $element = '*';
        }

        foreach ($attributes as $name) {
            if (isset(self::$attributes[$element][$name])) {
                self::$attributes[$element][$name] = null;
            }
        }
    }

    /**
     * Open new form, by default POST and "application/x-www-form-urlencoded"
     *
     * @param string|array $url default action url, may passed with array ['url' => ] or ['action' => ]
     * @param array $options List of additional options like
     * @return FormBuilder
     */
    public static function open($url = null, $options = [])
    {
        $attr = array_merge([
            'action' => call_user_func(function() use ($url) {
                if (isset($url['url'])) {
                    return $url['url'];
                } elseif (isset($url['action'])) {
                    unset($url['action']);
                    return $url['action'];
                } elseif (is_string($url)) {
                    return (string) $url;
                } else {
                    return '';
                }
            }),
            'method' => 'post',
            'enctype' => 'application/x-www-form-urlencoded'
        ], $options);

        return self::buildTag('form', $attr);
    }

    /**
     * Close the form through </form> tag
     *
     * @return FormBuilder
     */
    public static function close()
    {
        return self::buildTag('form', [], true);
    }

    /**
     * Build a custom filed, based on <input>
     *
     * @param string $name Field name
     * @param string $value Value contents
     * @param string $type One of the default input types, such as 'text', 'checkbox', 'radio', 'password', etc.
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function input($name, $value = null, $type = 'text', $attr = [])
    {
        return self::buildElement('input', array_merge(array_filter([
                'name' => $name,
                'value' => $value,
                'type' => $type
            ]), $attr), null, true);
    }

    /**
     * Build a HTML password input
     *
     * @param string $name Field name
     * @param string $value Value contents
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function password($name, $value = null, $attr = [])
    {
        return self::input($name, $value, 'password', $attr);
    }

    /**
     * Build a HTML checkbox input
     *
     * @param string $name Field name
     * @param string $value Value contents
     * @param bool $checked Set checked status
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function checkbox($name, $value = null, $checked = false, $attr = [])
    {
        $attr = array_merge(array_filter(['checked' => (bool) $checked]), $attr);
        return self::input($name, $value, 'checkbox', $attr);
    }

    /**
     * Build a HTML radio input
     *
     * @param string $name Field name
     * @param string $value Value contents
     * @param bool $checked Set checked status
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function radio($name, $value = null, $checked = false, $attr = [])
    {
        $attr = array_merge(array_filter(['checked' => (bool) $checked]), $attr);
        return self::input($name, $value, 'radio', $attr);
    }

    /**
     * Build a HTML file input
     *
     * @param string $name Field name
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function file($name, $attr = [])
    {
        return self::input($name, null, 'file', $attr);
    }

    /**
     * Build a input
     *
     * @param string $name Field name
     * @param string $value Value contents
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function hidden($name, $value = null, $attr = [])
    {
        return self::input($name, $value, 'hidden', $attr);
    }

    /**
     * Build a text input field
     *
     * @param string $name Field name
     * @param string $value Value contents
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function text($name, $value = null, $attr = [])
    {
        return self::input($name, $value, 'text', $attr);
    }

    /**
     * Build a HTML textarea field
     *
     * @param string $name Field name
     * @param string $value Value contents
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function textarea($name, $value = null, $attr = [])
    {
        return self::buildElement('textarea', array_merge(array_filter([
                'name' => $name,
                'rows' => 3,
                'cols' => 50,
            ]), $attr), $value);
    }

    /**
     * Build a HTML select field
     *
     * @param string $name Field name
     * @param array $options List of options, either as ['value' => 'display text'] or ['label' => ['value' => 'display text']]
     * @param string $default Value of selected option as default
     * @param array $attr Additional HTML attributes
     * @return FormBuilder
     */
    public static function select($name, $options = null, $default = null, $attr = [])
    {
        $attr = (array) $attr;
        $attr['name'] = $name;
        self::$output = '<select ' . self::buildAttributes('select', $attr) . '>';

        foreach ($options as $value => $display) {
            if (!is_array($display)) {
                $selected = $value === $default ? ' selected="selected"' : '';
                self::$output .= '<option value="' . self::escape($value) . '"';
                self::$output .= $selected . '>' . self::escape($display) . "</option>\n";
                continue;
            }

            self::$output .= '<optgroup label="' . self::escape($value) . "\">\n";
            foreach ($display as $optValue => $optDisplay) {
                $selected = $optValue === $default ? ' selected="selected"' : '';
                self::$output .= '<option value="' . self::escape($optValue) . '"';
                self::$output .= $selected . '>' . self::escape($optDisplay) . "</option>\n";
            }
            self::$output .= '</optgroup>';

        }

        self::$output .= '</select>';

        return new static();
    }

    /**
     * Build an HTML <button>
     *
     * @param string $name Field name
     * @param string|array|Form Label for the button. May contain child element or multiple parts through array
     * @param string $value Default button's value
     * @param array $attr Additional attributes
     */
    public static function button($name, $label = 'Button', $value = null, $attr = [])
    {
        $attr = array_merge([
            'name' => $name,
            'value' => $value,
            'type' => 'button'
        ], $attr);
        return self::buildElement('button', $attr, $label, false);
    }

    /**
     * Build an HTML submit through <button>
     *
     * @param string $name Field name
     * @param string|array|Form $label Label for the button. May contain child element or multiple parts through array
     * @param string $value Default button's value
     * @param array $attr Additional attributes
     */
    public static function submit($name, $label = 'Submit', $value = null, $attr = [])
    {
        return self::button($name, $label, $value, array_merge(['type' => 'submit'], $attr));
    }

    /**
     * Build an HTML reset through <button>
     *
     * @param string $name Field name
     * @param string|array|Form Label for the button. May contain child element or multiple parts through array
     * @param string $value Default button's value
     * @param array $attr Additional attributes
     */
    public static function reset($name, $label = 'Reset', $value = null, $attr = [])
    {
        return self::button($name, $label, $value, array_merge(['type' => 'reset'], $attr));
    }

    /**
     * Build an HTML label
     *
     * @param string|array|Form $label Label text. May contain child element or multiple parts through array
     * @param string $forName Adds for="" attribute
     * @param array $attr Additional attributes
     */
    public static function label($label = '', $forName = '',  $attr = [])
    {
        return self::buildElement('label', array_merge(array_filter(['for' => $forName]), $attr), $label);
    }

    /**
     * Escape input string
     *
     * @param string $input Input string
     * @return string
     */
    public static function escape($input)
    {
        return htmlentities($input, ENT_QUOTES, 'UTF-8', false);
    }

    /**
     * Print last generated element
     *
     * @return string
     */
    public function __toString()
    {
        return self::$output;
    }

    /**
     * Build a HTML attributes name-value pairs (like href="./../url/")
     *
     * @param string $name HTML Tag name
     * @param array $attr HTML attributes
     * @return string
     */
    protected static function buildAttributes($name, array $attr = [])
    {
        $attr = array_filter(array_merge(self::getAttributes($name), $attr));
        if (!count($attr)) {
            return '';
        }

        $output = [];
        foreach ($attr as $name => $value) {
            if ($value !== null) {
                $output[] = $name . '="' . self::escape($value) . '"';
            } else {
                $output[] = $name;
            }
        }
        return implode(' ', $output);
    }

    /**
     * Build a HTML element (i.e. both a <tagName /> or <tagName>...<tagName>)
     *
     * @param string $name HTML Tag name
     * @param array $attr Additional HTML attributes
     * @param string|array|Form $content Content between open and closed tags, can be another Form tag or even array.
     * @param bool $selfClosed If true, an "<tagName />"-variant is used, ignoring the $content value
     * @return FormBuilder
     */
    protected static function buildElement($name, $attr, $content = null, $selfClosed = false)
    {
        $attr = self::buildAttributes($name, $attr);
        $name = mb_strtolower($name, 'UTF-8');

        if ($selfClosed) {
            self::$output = '<' . $name . ' ' . $attr . ' />';
        } else {
            $content = (array) $content;
            $textNode = [];
            foreach ($content as $item) {
                if ($item instanceof FormBuilder) {
                    $textNode[] = (string) $item;
                } elseif (is_string($item) || is_numeric($item)) {
                    $textNode[] = (string) $item;
                } elseif (is_callable($item)) {
                    $textNode[] = (string) call_user_func($item);
                }
            }

            self::$output = '<' . $name . ' ' . $attr . '>'
                . implode(' ', $textNode)
                . '</' . $name . '>';
        }

        return new static();
    }

    /**
     * Build a HTML tag (i.e. either a <tagName> or </tagName> but not both)
     *
     * @param string $name HTML Tag name
     * @param array $attr Additional HTML attributes
     * @param bool $closing Choose between open tag on True (i.e. <tagName>), or closing tag on false (i.e. </tagName>)
     * @return FormBuilder
     */
    protected static function buildTag($name, array $attr = [], $closing = false)
    {
        $attr = self::buildAttributes($name, $attr);
        $name = mb_strtolower($name, 'UTF-8');

        if (!$closing) {
            self::$output = '<' . $name . ' ' . $attr . '>';
        } else {
            self::$output = '</' . $name . '>';
        }

        return new static();
    }
}
