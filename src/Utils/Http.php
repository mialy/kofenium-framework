<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium\Utils;

use Carbon\Carbon;
use Kofenium\Application as App;

/**
 * Http utilities like URL generation or redirecting
 *
 */
class Http
{
    /**
     * Get the application's framework default instance
     *
     * @return \Kofenium\Application
     */
    protected static function app()
    {
        return App::getInstance();
    }

    /**
     * Get url from the router's name
     *
     * @param string $routerName Router name
     * @param array $params Parameters to the router address (sush as '/user/[i:id])
     * @param string $default The default URL if the router name not found
     */
    public static function action($routerName, $params = [], $default = '/')
    {
        try {
            return self::app()
                ->getRouter()
                ->generate($routerName, $params);
        } catch (\Exception $e) {
            return $default;
        }
    }

    /**
     * Redirect to URL
     *
     * @param string $address URL address
     * @param int $code HTTP code for redirection
     */
    public static function redirect($address, $code = 302)
    {
        header("Location: " . $address, true, $code);
        exit;
    }

    /**
     * Redirect to URL, based on Router's name
     *
     * @param string $routerName Router name
     * @param array $params Parameters to the router address (sush as '/user/[i:id])
     * @param int $code HTTP code for redirection
     */
    public static function redirectRoute($routerName, $params = [], $code = 302)
    {
        header("Location: " . self::action($routerName, $params), true, $code);
        exit;
    }

    /**
     * Get absolute URL to public assets, with additional version string,
     * based on file's mtime
     *
     * @param string $file The Asset resource w/ relative path to HTDOCS folder
     * @param string $fileDebugMode Another asset resource, in case if debug mode is enabled
     * @return string
     */
    public static function asset($file = null, $fileDebugMode = null)
    {
        $app = Http::app();
        $debugMode = $app->getConfig()->get('app.debug', false, 'bool');

        $file = mb_strlen($fileDebugMode, 'UTF-8') && $debugMode === true
            ? $fileDebugMode
            : $file;

        $file = str_replace(DIRECTORY_SEPARATOR, '/', $file);
        $file = ltrim($file, '/');
        $basePath = $app->getDocumentRoot() . DIRECTORY_SEPARATOR;

        $filePath = realpath($basePath . $file);

        $version = Carbon::now()->format('Ymd');
        if ($filePath && is_readable($filePath) && is_file($filePath)) {
            clearstatcache();
            $version = @filemtime($filePath);
            $version = Carbon::createFromTimeStamp($version)->format('YmdHis');
        }

        return $app->getBaseUrl() . $file . '?version=' . $version;
    }
}
