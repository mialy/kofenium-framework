<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use Closure;
use Kofenium\Singleton;
use InvalidArgumentException;

/**
 * Events during the application execution
 */
class Event extends Singleton
{
    /**
     * List of all events and their methods
     *
     * @var array
     */
    protected static $events = [];

    /**
     * Attach event listener(s)
     *
     * @param array|string $events The name or list of names for the events
     * @param callable|string $callback Either a closure, or a "\NameSpace\To\ClassName@methodName" string.
     *    To stop Propagation - the callback must returns false.
     * @param int $priority Sets priority level, from 0 as the lowest priority
     * @param bool $isFirst Sets listener as first in the selected priority
     * @return Event
     * @throws \InvalidArgumentException On invalid event name
     */
    public static function on($events, $callback, $priority = 0, $isFirst = false)
    {
        $priority = (int) $priority;
        $priority = $priority < 0 ? 0 : $priority;

        foreach ((array) $events as $event) {
            if (!is_string($event)) {
                throw new InvalidArgumentException('Expected string or array of strings argument in $events. Passed: ' . $event);
            }

            if (is_string($callback)) {
                $callback = explode('@', $callback);

                if (count($callback) == 2) {
                    $callback = [
                        $callback[0], // class name
                        $callback[1], // method
                    ];
                }
            }

            if (!is_callable($callback)) {
                throw new InvalidArgumentException('Expected string or callable as callback.');
            }

            if (is_array($callback) && count($callback) === 2) {
                $callback = implode('@', $callback);
            }

            if (!$isFirst || empty(self::$events[$event][$priority])) {
                self::$events[$event][$priority][] = $callback;
            } else {
                array_unshift(self::$events[$event][$priority], $callback);
            }
        }

        return self::getInstance();
    }

    /**
     * De-attach event listener(s)
     *
     * @param array|string $events The name or list of names for the events
     * @param string $callback Callback as string in format "\NameSpace\To\ClassName@methodName".
     *   Closures will not be accepted.
     * @param int $priority Specify listener priority. -1 to unbind all listeners
     * @return Event
     * @throws \InvalidArgumentException On invalid event name
     */
    public static function off($events, $callback = null, $priority = -1)
    {
        foreach ((array) $events as $event) {
            if (!is_string($event)) {
                throw new InvalidArgumentException('Expected string or array of strings argument in $events. Passed: ' . $event);
            }

            if (empty(self::$events[$event])) {
                continue;
            }

            if (!$callback) {
                if ($priority >= 0) {
                    unset(self::$events[$event][$priority]);
                    continue;
                }

                foreach (self::$events[$event] as $level => $list) {
                    unset(self::$events[$event][$level]);
                }

                return self::getInstance();

            } elseif (!is_string($callback)) {
                throw new InvalidArgumentException('Expected string as callback.');
            }

            foreach (self::$events[$event] as $level => $list) {
                if ($priority >= 0 && $level !== $priority) {
                    continue;
                }

                foreach ($list as $item => $contents) {
                    if (!is_string($contents) || $contents !== $callback) {
                        continue;
                    }

                    unset(self::$events[$event][$level][$item]);
                }
            }
        }

        return self::getInstance();
    }

    /**
     * Fire/trigger the event listener(s)
     *
     * @param array|string $events The name or list of names for the events
     * @param array $parameters Additional parameters to the listener callback
     * @param bool $reverseOrder If true, the return array will be ordered in backwards,
     *   i.e. last called events and last called callbacks are first items in the array
     * @return array Array list of returned values of each listener
     * @throws \InvalidArgumentException On invalid event name
     */
    public static function trigger($events, $parameters = [], $reverseOrder = true)
    {
        $result = [];

        foreach ((array) $events as $event) {
            if (!is_string($event)) {
                throw new InvalidArgumentException('Expected string or array of strings argument in $events. Passed: ' . $event);
            }

            if (!isset(self::$events[$event]) || !is_array(self::$events[$event])) {
                continue;
            }

            $list = self::$events[$event];
            // sort by priority
            krsort($list);

            foreach ($list as $callbacks) {
                foreach ($callbacks as $callback) {
                    // \Namespace\To\ClassName@methodName
                    if (is_string($callback) && strpos($callback, '@') !== false) {
                        $callback = explode('@', $callback);
                        $callback = [
                            new $callback[0](),
                            $callback[1],
                        ];
                    }

                    $return = call_user_func_array($callback, $parameters);
                    $result[$event][] = $return;
                    if ($return === false) {
                        break;
                    }
                }
            }

            if ($reverseOrder) {
                $result[$event] = array_reverse($result[$event], false);
            }
        }

        if ($reverseOrder) {
            $result = array_reverse($result, true);
        }

        return $result;
    }
}
