<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use PDO;
use Exception;
use Kofenium\Application;
use Kofenium\ValidateData;
use Kofenium\Database\Database;

/**
 * Model controller
 */
abstract class Model
{
    /**
     * Application instance
     *
     * @var \Kofenium\Application
     */
    protected $app = null;

    /**
     * Config instance
     *
     * @var \Kofenium\Config
     */
    protected $config = null;

    /**
     * Database connection instance
     *
     * @var \Kofenium\Database\Database
     */
    protected $db = null;

    /**
     * Default connection name
     *
     * @var string
     */
    protected $connection = null;

    /**
     * Table name prefix
     *
     * @var string
     */
    protected $tablePrefix = '';

    /**
     * Model's default table
     *
     * @var string
     */
    protected $table = '';

    /**
     * The column primary key, used for default orderings
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Rules for the validation through \Kofenium\ValidateData
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Validation errors list, each key is the rule group (or FORM field)
     *
     * @var array
     */
    protected $validationErrors = [];

    /**
     * Last query error
     *
     * @var string
     */
    protected $error = '';

    /**
     * Init the properties
     */
    public function __construct()
    {
        $this->app = Application::getInstance();
        $this->config = $this->app->getConfig();

        if ($this->connection === null) {
            $this->connection = $this->config->get('database.default', null);
        }
        $this->db = new Database($this->app->getDatabaseConnection($this->connection));

        if (empty($this->tablePrefix)) {
            $prefix = 'database.connections.' . $this->connection . '.prefix';
            $this->tablePrefix = $this->config->get($prefix, $this->tablePrefix, 'trim');
        }

        $this->table = $this->tablePrefix . $this->table;
    }

    /**
     * Get Single row from database
     *
     * @param mixed $id Unique ID from Primary Key
     * @param integer $fetchAs Fetch result as PDO::FETCH_ASSOC, PDO::FETCH_NUM or PDO::FETCH_OBJ
     * @return stdClass|array|bool Result as stdClass or array (depends on $fetchAs), or false on error
     */
    public function get($id, $fetchAs = 0)
    {
        list($key, $value) = $this->determinePrimaryKey($id);

        $sql = "
            SELECT *
            FROM " . $this->table . "
            WHERE " . $key . " = :id
        ";

        try {
            $result = $this->db
                ->prepare($sql)
                ->execute(['id' => $value])
                ->fetch($fetchAs);
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        return $result;
    }

    /**
     * Delete data row from the database
     *
     * @param int|string|array $id PrimaryKey unique identifier. Also key-value array is accepted
     * @return bool|int False on error or number of deleted rows
     */
    public function delete($id)
    {
        list($key, $value) = $this->determinePrimaryKey($id);

        $sql = "
            DELETE FROM " . $this->table . "
            WHERE " . $key . " = :id
        ";

        try {
            $result = $this->db
                ->prepare($sql)
                ->execute(['id' => $value])
                ->getAffectedRows();
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        return $result;
    }

    /**
     * Get number of rows
     *
     * @return integer|bool Count as integer, or false on error
     */
    public function getCount()
    {
        $sql = "
            SELECT COUNT(*)
            FROM " . $this->table . "
            WHERE 1
        ";

        try {
            $count = (int) $this->db
                ->prepare($sql)
                ->execute()
                ->fetchColumn(0);
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        return $count;
    }

    /**
     * Get table column names and types
     *
     * @param integer $fetchAs Fetch result as PDO::FETCH_ASSOC, PDO::FETCH_NUM, PDO::FETCH_BOTH or PDO::FETCH_OBJ
     * @return array
     */
    public function getColumnsList($fetchAs = 0)
    {
        $sql = "
            DESCRIBE " . $this->table . "
        ";

        try {
            $result = $this->db
                ->prepare($sql)
                ->execute()
                ->fetchAll($fetchAs);
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        return $result;
    }

    /**
     * Get last error
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Get validation errors
     *
     * @return array
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * Get all rows from database, ordered and filtered by conditions
     *
     * @param array $options Additional options as key-value array, see below:
     * <pre>
     * total = How many rows to be fetched, null or -1 for no restriction
     * offset = Start from which rows
     * order = Array of order fields
     * where = Conditions array
     * </pre>
     * @param integer $fetchAs Fetch result as PDO::FETCH_ASSOC, PDO::FETCH_NUM, PDO::FETCH_BOTH or PDO::FETCH_OBJ
     * @return stdClass|array|bool False on error, array of object - on result found
     * @todo Implement the code
     */
    public function getAll($options = [], $fetchAs = 0)
    {
    }

    /**
     * Save the data into database. If there is row with the same UID, the data will be updated,
     * and vice versa - if data is new, it will be inserted as new row
     *
     * @param array $data Row data input to be inserted/updated
     * @param bool $disableValidation If true - disable validation before update/insert data
     * @return bool True on success, false - otherwise
     */
    public function save($data, $disableValidation = false)
    {
        if ($this->isDataExists($data)) {
            return $this->update($data, $disableValidation);
        } else {
            return $this->create($data, $disableValidation);
        }
    }

    /**
     * Insert data into database
     *
     * @param array $data Row Data to be inserted
     * @param bool $disableValidation If true - disable data validation before insert
     * @return bool True on success, false - otherwise
     */
    public function create($data, $disableValidation = false)
    {
        if (!$disableValidation && !$this->isValid($data)) {
            return false;
        }

        $columns = $this->getColumnsList(PDO::FETCH_NUM);

        $columnsList = [];
        $values = [];

        foreach ($columns as $column) {
            $name = $column[0];
            if ($name === $this->primaryKey) {
                continue;
            }

            $columnsList[] = $name;
            $values[':' . $name] = isset($data[$name]) ? $data[$name] : '';
        }

        $sql = "INSERT INTO " . $this->table . " (
                    " . implode(", ", $columnsList) . "
                ) VALUES (
                    " . implode(', ', array_keys($values)) . "
                );";
        try {
            $result = $this->db
                ->prepare($sql)
                ->execute($values)
                ->getAffectedRows() > 0;
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        return $result;
    }

    /**
     * Update data from the database
     *
     * @param array $data Row Data to be inserted
     * @param bool $disableValidation If true - disable data validation before update
     * @return bool True on success, false - otherwise
     */
    public function update($data, $disableValidation = false)
    {
        if (!$disableValidation && !$this->isValid($data)) {
            return false;
        }

        if (!isset($data[$this->primaryKey])) {
            return false;
        }

        if (($oldData = $this->get($data[$this->primaryKey])) === false) {
            return false;
        }

        ksort($oldData);
        ksort($data);
        $columns = $this->getColumnsList(PDO::FETCH_NUM);

        $values = [];
        $fields = [];
        foreach ($columns as $column) {
            $name = $column[0];
            if ($name === $this->primaryKey) {
                $values[':' . $name] = $data[$name];
                continue;
            } elseif (!isset($data[$name])) {
                unset($oldData[$name]);
                continue;
            }

            $fields[] = $name . ' = :' . $name;
            $values[':' . $name] = $data[$name];
        }

        $sql = "UPDATE " . $this->table . "
                SET
                    " . implode(', ', $fields) . "
                WHERE " . $this->primaryKey . " = :" . $this->primaryKey . "
                LIMIT 1";
        try {
            $result = $this->db
                ->prepare($sql)
                ->execute($values)
                ->getAffectedRows() > 0;
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        // getAffectedRows may return false when data is actually updated successfully,
        // but there is no differences between old and new data
        if (!$result && !array_diff($oldData, $data)) {
            return true;
        }

        return $result;
    }


    /**
     * Determine if data exists into database
     *
     * @param array $data Data to check
     * @return bool True on exists, false - otherwise
     */
    public function isDataExists($data)
    {
        if (!isset($data[$this->primaryKey])) {
            return false;
        }

        if ($this->get($data[$this->primaryKey]) === false) {
            return false;
        }

        return true;
    }

    /**
     * Find the primary key and the value from the input
     *
     * @param int|string|array $id PrimaryKey unique identifier. Also key-value array is accepted
     * @return array [0] => keyName, [1] = value
     */
    public function determinePrimaryKey($input)
    {
        if (is_array($input) && count($input) > 0) {
            $key = array_keys($input)[0];
            $value = $input[$key];
        } else {
            $key = $this->primaryKey;
            $value = $input;
        }

        return [$key, $value];
    }

    /**
     * Validate data before insert/update
     *
     * @param array $data List of data
     * @param array $rules Custom rules to check, if not provided the property rules will be used
     * @return bool True if data is valid, false - otherwise
     */
    public function isValid($data, $rules = null)
    {
        $rules = empty($rules) || !is_array($rules) || !count($rules)
            ? $this->rules
            : $rules;

        if ($rules === null || !is_array($rules) || !count($rules)) {
            return true;
        }

        $validData = new ValidateData();
        $validData->setRules($rules);

        foreach ($rules as $group => $rule) {
            if (!isset($data[$group])) {
                continue;
            }
            $validData->setValue($group, $data[$group]);
        }

        $this->validationErrors = [];
        if (!$validData->run()) {
            $this->validationErrors = $validData->getErrors();
            $this->error = "Invalid data";
            return false;
        }

        return true;
    }
}
