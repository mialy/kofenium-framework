<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

/**
 * Change (cast) input data in different ways
 *
 */
class FilterData
{
    /**
     * Normalize input data, based on requested filters
     *
     * List of current supported filters:
     * <ul>
     *  <li><b>bool</b> or <b>boolean</b> - cast to boolean</li>
     *  <li><b>int</b> or <b>integer</b> - cast to integer</li>
     *  <li><b>float</b> - cast to float</li>
     *  <li><b>double</b> - cast to double</li>
     *  <li><b>string</b> - cast to integer</li>
     *  <li><b>ltrim</b>, <b>rtrim</b> and <b>trim</b> - Strip whitespace from left, right, or both Sides</li>
     *  <li><b>lower</b>, <b>lowercase</b> - Lower case string</li>
     *  <li><b>upper</b>, <b>uppercase</b> - Upper case string</li>
     *  <li><b>camelcase</b>, <b>titlecase</b>, <b>pascalcase</b> - UpperCase string</li>
     *  <li><b>snakecase</b> - lower_snake_case string</li>
     *  <li><b>lowercamelcase</b> - Like camelcase, but first letter is also lower</li>
     * </ul>
     *
     * @param mixed $data Input data
     * @param string|array $filters List of filters as array or string of pipe-separated filters
     * @return mixed The input data, after normalization thourgh all filters
     */
    public static function normalize($data, $filters = null)
    {
        if (is_array($filters) && count($filters)) {
            $list = $filters;
        } elseif (is_string($filters)) {
            $list = explode('|', strtolower($filters));
            if (!is_array($list) || !count($list)) {
                return $data;
            }
        } else {
            return $data;
        }

        foreach ($list as $filter) {
            switch ($filter) {
                case 'int':
                case 'integer':
                    $data = (int) $data;
                    break;

                case 'string':
                    $data = (string) $data;
                    break;

                case 'float':
                    $data = (float) $data;
                    break;

                case 'double':
                    $data = (double) $data;
                    break;

                case 'bool':
                case 'boolean':
                    $data = (bool) $data;
                    break;

                case 'trim':
                case 'rtrim':
                case 'ltrim':
                    $data = $filter($data);
                    break;

                case 'titlecase':
                case 'camelcase':
                case 'pascalcase':
                    $data = self::toCamelCase($data);
                    break;

                case 'lowercamelcase':
                    $data = self::toCamelCase($data);
                    if (isset($data[0])) {
                         $value[0] = mb_strtolower($data[0]);
                    }
                    break;

                case 'snakecase':
                    $data = self::toSnakeCase($data);
                    break;

                case 'lower':
                case 'lowercase':
                    $data = mb_strtolower($data);
                    break;

                case 'upper':
                case 'uppercase':
                    $data = mb_strtoupper($data);
                    break;
            }
        }

        return $data;
    }

    /**
     * Convert string from CamelCase to snake_case
     *
     * @param string $str Input string
     * @param string $separator Separator between words
     * @return string
     */
    public static function toSnakeCase($str, $separator = '_')
    {
        $value = mb_strtolower(preg_replace('/(.)(?=[\p{Lu}])/u', '$1'.$separator, $str));
        $value = preg_replace('/\s+/u', '', $value);
        $value = preg_replace('/_+/u', '_', $value);
        return $value;
    }

    /**
     * Convert string from snake_case to CamelCase
     *
     * @param string $str Input string
     * @param bool $lowerFirst Determine the first letter to be lower or upper
     * @return string
     */
    public static function toCamelCase($str, $lowerFirst = false)
    {
        $value = trim(preg_replace('/[^\p{L}\d]+/iu', ' ', $str));
        $value = mb_convert_case($value, MB_CASE_TITLE, 'UTF-8');
        $value = preg_replace('/\s+/u', '', $value);

        if ($lowerFirst && isset($value[0])) {
            $value[0] = mb_strtolower($value[0]);
        }

        return $value;
    }
}
