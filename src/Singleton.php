<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

/**
 * Singleton pattern abstract class
 */
abstract class Singleton
{
    /**
     * Singleton class instances list
     *
     * @var array
     */
    private static $instances;

    /**
     * Get instance of the inherited class
     *
     * @return Singleton
     */
    final public static function getInstance()
    {
        $class = get_called_class();

        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new static();
        }

        return self::$instances[$class];
    }

    /**
     * Prevent outside instances
     */
    protected function __construct()
    {

    }

    /**
     * Prevent cloning
     */
    private function __clone()
    {

    }

    /**
     * Prevent from unserialization
     */
    private function __wakeup()
    {

    }
}
