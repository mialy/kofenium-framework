<?php
/**
 * Kofenium framework
 *
 * PHP Version 5.4
 */

namespace Kofenium;

use SplFileInfo;

/**
 * Additional info for uploaded files
 *
 * @see \Kofenium\Input::file()
 */
class UploadedFile extends SplFileInfo
{
    /**
     * File length
     *
     * @var int
     */
    protected $size;

    /**
     * Error code
     *
     * @var int
     */
    protected $error;

    /**
     * MIME type
     *
     * @var string
     */
    protected $mimeType;

    /**
     * Original filename
     *
     * @var string
     */
    protected $originalName;

    /**
     * Constructor
     *
     * @param string $path Full path to the uploaded file
     * @param string $name Original filename during upload
     * @param int $size File size
     * @param string $mime MIME-type of the file
     * @param int $error Status error
     */
    public function __construct($path, $name, $size = null, $mime = null, $error = null)
    {
        $this->originalName = $this->getName($name);
        $this->mimeType = $mime ?: 'application/octet-stream';
        $this->size = (int) $size;
        $this->error = $error ?: UPLOAD_ERR_OK;
        parent::__construct($path);
    }

    /**
     * Get Original filename
     *
     * @return string
     */
    public function getUploadedName()
    {
        return $this->originalName;
    }

    /**
     * Get Original file extension
     *
     * @return string
     */
    public function getUploadedExtension()
    {
        return pathinfo($this->originalName, PATHINFO_EXTENSION);
    }

    /**
     * Get Original file MIME type
     *
     * @return string
     */
    public function getUploadedMime()
    {
        return $this->mimeType;
    }

    /**
     * Get Original file size
     *
     * @return int
     */
    public function getUploadedSize()
    {
        return $this->size;
    }

    /**
     * Get Error code during upload
     *
     * @return int
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Check if the uploading is successfull
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->error === UPLOAD_ERR_OK
            && is_uploaded_file($this->getPathname());
    }

    /**
     * Get filename from the full path
     *
     * @param string $name Source name
     * @return string
     */
    protected function getName($name)
    {
        $originalName = str_replace('\\', '/', $name);
        $pos = strrpos($originalName, '/');
        $originalName = false === $pos ? $originalName
            : substr($originalName, $pos + 1);

        return $originalName;
    }
}
